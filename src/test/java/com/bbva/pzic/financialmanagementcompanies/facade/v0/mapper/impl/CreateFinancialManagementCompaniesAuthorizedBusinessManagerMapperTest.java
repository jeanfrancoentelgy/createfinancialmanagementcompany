package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class CreateFinancialManagementCompaniesAuthorizedBusinessManagerMapperTest {

    private static final String AUTHORIZED_BUSINESS_MANAGER_SIGNATURE_ID_BACKEND = "2";
    private static final String AUTHORIZED_BUSINESS_MANAGER_OPERATIONS_RIGHTS_PERMISSION_TYPE_ID_BACKEND = "C";
    private static final String AUTHORIZED_BUSINESS_MANAGER_STATUS = "A1";

    @InjectMocks
    private CreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper mapper;
    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void mapInFullTest() throws IOException {
        CreateAuthorizedBusinessManager input = EntityStubs.getInstance().getCreateAuthorizedBusinessManager();

        when(enumMapper.getBackendValue("authorizedBusinessManager.signature.id",
                input.getBusinessManagement().getOperationsRights().getSignature().getValidationRights().getId())).thenReturn(AUTHORIZED_BUSINESS_MANAGER_SIGNATURE_ID_BACKEND);
        when(enumMapper.getBackendValue("authorizedBusinessManager.operationsRights.permissionType.id",
                input.getBusinessManagement().getOperationsRights().getPermissionType().getId())).thenReturn(AUTHORIZED_BUSINESS_MANAGER_OPERATIONS_RIGHTS_PERMISSION_TYPE_ID_BACKEND);

        when(enumMapper.getBackendValue("authorizedBusinessManager.status",
                input.getBusinessManagement().getStatus())).thenReturn(AUTHORIZED_BUSINESS_MANAGER_STATUS);


        InputCreateAuthorizedBusinessManager result = mapper.mapIn(FINANCIAL_MANAGEMENT_COMPANY_ID, input);

        assertNotNull(result);
        assertNotNull(result.getFinancialManagementCompanyId());
        assertNotNull(result.getBusinessManagerId());
        assertNotNull(result.getValidationRightsId());
        assertNotNull(result.getPermissionTypeId());
        assertNotNull(result.getBusinessManagementStatus());

        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_ID, result.getFinancialManagementCompanyId());
        assertEquals(input.getBusinessManager().getId(), result.getBusinessManagerId());
        assertEquals(AUTHORIZED_BUSINESS_MANAGER_SIGNATURE_ID_BACKEND, result.getValidationRightsId());
        assertEquals(AUTHORIZED_BUSINESS_MANAGER_OPERATIONS_RIGHTS_PERMISSION_TYPE_ID_BACKEND, result.getPermissionTypeId());
        assertEquals(AUTHORIZED_BUSINESS_MANAGER_STATUS, result.getBusinessManagementStatus());
    }

    @Test
    public void mapInEmptyTest() {
        InputCreateAuthorizedBusinessManager result = mapper.mapIn(FINANCIAL_MANAGEMENT_COMPANY_ID, new CreateAuthorizedBusinessManager());
        assertNotNull(result);
        assertNotNull(result.getFinancialManagementCompanyId());
        assertNull(result.getBusinessManagerId());
        assertNull(result.getValidationRightsId());
        assertNull(result.getPermissionTypeId());
        assertNull(result.getBusinessManagementStatus());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<CreateAuthorizedBusinessManager> result = mapper.mapOut(new CreateAuthorizedBusinessManager());
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<CreateAuthorizedBusinessManager> result = mapper.mapOut(null);
        assertNull(result);
    }
}
