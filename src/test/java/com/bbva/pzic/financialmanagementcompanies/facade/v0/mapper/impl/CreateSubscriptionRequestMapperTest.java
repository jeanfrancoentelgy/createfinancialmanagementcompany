package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateSubscriptionRequestMapperTest {

    @InjectMocks
    private CreateSubscriptionRequestMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        mapBackendValues();
    }

    private void mapBackendValues() {
        when(enumMapper.getBackendValue("subscriptionRequests.documentType.id", DNI_DOCUMENT_TYPE_ID_ENUM)).thenReturn(DNI_DOCUMENT_TYPE_ID_BACKEND);
        when(enumMapper.getBackendValue("subscriptionRequests.documentType.id", RUC_DOCUMENT_TYPE_ID_ENUM)).thenReturn(RUC_DOCUMENT_TYPE_ID_BACKEND);
        when(enumMapper.getBackendValue("campaigns.offers.productType", ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_ENUM)).thenReturn(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND);
        when(enumMapper.getBackendValue("loans.relatedContracts.relationType", PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_ENUM)).thenReturn(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND);
        when(enumMapper.getBackendValue("suscriptionRequest.product.id", PRODUCT_ID_ENUM)).thenReturn(PRODUCT_ID_BACKEND);
        when(enumMapper.getBackendValue("joint.id", JOINT_SIGNATURE_OF_TWO_JOINT_ID_ENUM)).thenReturn(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND);
        when(enumMapper.getBackendValue("contactDetails.contactType.id", EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_ENUM)).thenReturn(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND);
        when(enumMapper.getBackendValue("businessManagers.role.id", LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_ENUM)).thenReturn(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND);
    }

    @Test
    public void mapInFullTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance().getSubscriptionRequest();

        when(enumMapper.getBackendValue("financialManagementCompany.businessManagement.managementType.id",
                input.getBusiness().getBusinessManagement().getManagementType().getId())).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_BACKEND);
        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0).getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0).getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles().get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness().getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_BACKEND, result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertEquals(input.getBusiness().getLegalName(), result.getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result.getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result.getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract().getId(), result.getRelatedContracts().get(0).getContract().getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct().getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(), result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(), result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(), result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0).getSecondLastName(), result.getBusinessManagers().get(0).getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND, result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getBusinessManagers().get(0).getContactDetails().get(0).getContactType());
        assertEquals(input.getBusinessManagers().get(0).getContactDetails().get(0).getContact(), result.getBusinessManagers().get(0).getContactDetails().get(0).getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0).getId());
        assertEquals(input.getUnitManagement(), result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch().getId());
    }

    @Test
    public void mapInWithoutBusinessDocumentTypeTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance().getSubscriptionRequest();
        input.getBusiness().getBusinessDocuments().get(0).setBusinessDocumentType(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());

        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutProductTypeTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getRelatedContracts().get(0).getProduct().setProductType(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());

        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutRelationTypeTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getRelatedContracts().get(0).setRelationType(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNull(result.getRelatedContracts().get(0)
                .getRelationType());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());

        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutProductTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.setProduct(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNull(result.getProduct());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());

        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());

        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInJointTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.setJoint(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNull(result.getJoint());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());

        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInBusinessManagerDocumentTypeTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusinessManagers().get(0).getIdentityDocuments().get(0).setDocumentType(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());

        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutIdAndLegalNameFromBusinessTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusiness().setId(null);
        input.getBusiness().setLegalName(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutBusinessDocumentsTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusiness().setBusinessDocuments(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNull(result.getBusiness().getBusinessDocuments());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());

        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutBusinessTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.setBusiness(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNull(result.getBusiness());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());


        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutRelatedContractsTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.setRelatedContracts(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNull(result.getRelatedContracts());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());

        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutBusinessManagersTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.setBusinessManagers(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNull(result.getBusinessManagers());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());

        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutFirstIdentityDocumentsFromBusinessManagersTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusinessManagers().get(0).setIdentityDocuments(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());

        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInFirstContactDetailsFromBusinessManagersTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusinessManagers().get(0).setContactDetails(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNull(result.getBusinessManagers().get(0)
                .getContactDetails());
        assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());

        assertEquals(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND, result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInFirstRoleFromBusinessManagerTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusinessManagers().get(0).setRoles(null);

        SubscriptionRequest result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        assertNull(result.getBusinessManagers().get(0).getRoles());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());

        assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelatedContracts().get(0)
                .getRelationType().getId());
        assertEquals(PRODUCT_ID_BACKEND, result.getProduct()
                .getId());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND, result.getJoint().getId());
        assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        assertEquals(DNI_DOCUMENT_TYPE_ID_BACKEND,
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND, result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());

        assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInEmptyTest() {
        SubscriptionRequest result = mapper
                .mapIn(new SubscriptionRequest());
        assertNotNull(result);
        assertNull(result.getBusiness());
        assertNull(result.getLimitAmount());
        assertNull(result.getRelatedContracts());
        assertNull(result.getProduct());
        assertNull(result.getJoint());
        assertNull(result.getBusinessManagers());
        assertNull(result.getUnitManagement());
        assertNull(result.getBranch());
    }
}
