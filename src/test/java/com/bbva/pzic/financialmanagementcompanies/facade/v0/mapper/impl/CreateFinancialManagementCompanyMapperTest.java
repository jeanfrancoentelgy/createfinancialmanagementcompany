package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 17/09/2020
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateFinancialManagementCompanyMapperTest {

    @InjectMocks
    private CreateFinancialManagementCompanyMapper mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() throws IOException {
        FinancialManagementCompanies input = EntityStubs.getInstance().getFinancialManagementCompanies();

        when(translator.translateFrontendEnumValueStrictly("documentType.id",
                input.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId()))
                .thenReturn(RUC_DOCUMENT_TYPE_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.businessManagement.managementType.id",
                input.getBusiness().getBusinessManagement().getManagementType().getId()))
                .thenReturn(MANAGEMENT_TYPE_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.product.id",
                input.getNetcashType().getId()))
                .thenReturn(SUBSCRIPTION_REQUEST_PRODUCT_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id",
                input.getNetcashType().getVersion().getId()))
                .thenReturn(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.productType.id",
                input.getProduct().getProductType().getId()))
                .thenReturn(PRODUCT_TYPE_BACKEND_D);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.relatedContracts.relationType",
                input.getRelationType().getId()))
                .thenReturn(RELATION_TYPE_BACKEND_C);
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id",
                input.getReviewers().get(0).getContactDetails().get(0).getContactType()))
                .thenReturn(CONTACT_DETAILS_CONTACT_TYPE_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.reviewer.id",
                input.getReviewers().get(0).getReviewerType().getId()))
                .thenReturn(SUSCRIPTION_REQUEST_REVIEWER_BACKEND);

        DTOIntFinancialManagementCompanies result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getLimitAmount().getAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
        assertNotNull(result.getNetcashType().getId());
        assertNotNull(result.getNetcashType().getVersion().getId());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getProductType().getId());
        assertNotNull(result.getRelationType().getId());
        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getUnitManagement());
        assertNotNull(result.getReviewers().get(0).getBank().getId());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
        assertNotNull(result.getReviewers().get(0).getProfile().getId());
        assertNotNull(result.getReviewers().get(0).getProfessionPosition());
        assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());

        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getExpirationDate(), result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getIssueDate(), result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertEquals(MANAGEMENT_TYPE_ID_BACKEND , result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertEquals(input.getBusiness().getLimitAmount().getAmount(), result.getBusiness().getLimitAmount().getAmount());
        assertEquals(input.getBusiness().getLimitAmount().getCurrency(), result.getBusiness().getLimitAmount().getCurrency());
        assertEquals(SUBSCRIPTION_REQUEST_PRODUCT_ID_BACKEND, result.getNetcashType().getId());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND, result.getNetcashType().getVersion().getId());
        assertEquals(input.getContract().getId(), result.getContract().getId());
        assertEquals(input.getProduct().getId(), result.getProduct().getId());
        assertEquals(PRODUCT_TYPE_BACKEND_D, result.getProduct().getProductType().getId());
        assertEquals(RELATION_TYPE_BACKEND_C, result.getRelationType().getId());
        assertEquals(input.getReviewers().get(0).getBusinessAgentId(), result.getReviewers().get(0).getBusinessAgentId());
        assertEquals(input.getReviewers().get(0).getContactDetails().get(0).getContact(), result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertEquals(CONTACT_DETAILS_CONTACT_TYPE_BACKEND, result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertEquals(SUSCRIPTION_REQUEST_REVIEWER_BACKEND, result.getReviewers().get(0).getReviewerType().getId());
        assertEquals(input.getReviewers().get(0).getUnitManagement(), result.getReviewers().get(0).getUnitManagement());
        assertEquals(input.getReviewers().get(0).getBank().getId(), result.getReviewers().get(0).getBank().getId());
        assertEquals(input.getReviewers().get(0).getBank().getBranch().getId(), result.getReviewers().get(0).getBank().getBranch().getId());
        assertEquals(input.getReviewers().get(0).getProfile().getId(), result.getReviewers().get(0).getProfile().getId());
        assertEquals(input.getReviewers().get(0).getProfessionPosition(), result.getReviewers().get(0).getProfessionPosition());
        assertEquals(input.getReviewers().get(0).getRegistrationIdentifier(), result.getReviewers().get(0).getRegistrationIdentifier());

    }

    @Test
    public void mapInEmptyTest()  {
        FinancialManagementCompanies input = new FinancialManagementCompanies();
        DTOIntFinancialManagementCompanies result = mapper.mapIn(input);
        assertNotNull(result);
        assertNull(result.getBusiness());
        assertNull(result.getNetcashType());
        assertNull(result.getContract());
        assertNull(result.getProduct());
        assertNull(result.getRelationType());
        assertNull(result.getReviewers());
    }

    @Test
    public void mapOutFullTest() {
        FinancialManagementCompanies financialManagementCompanies = new FinancialManagementCompanies();
        ServiceResponse<FinancialManagementCompanies> result = mapper.mapOut(financialManagementCompanies);
        assertNotNull(result);
        assertNotNull(result.getData());

        assertEquals(financialManagementCompanies, result.getData());

    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<FinancialManagementCompanies> result = mapper.mapOut(null);
        assertNull(result);
    }


}
