package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifyBusinessManagerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManagerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IModifyBusinessManagerSubscriptionRequestMapper;
import org.junit.Before;
import org.junit.Test;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.BUSINESS_MANAGER_ID;
import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.SUBSCRIPTION_REQUEST_ID;
import static org.junit.Assert.*;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public class ModifyBusinessManagerSubscriptionRequestMapperTest {

    private IModifyBusinessManagerSubscriptionRequestMapper mapper;

    @Before
    public void setUp() {
        mapper = new ModifyBusinessManagerSubscriptionRequestMapper();
    }

    @Test
    public void mapInFullTest() {
        BusinessManagerSubscriptionRequest input = EntityStubs.getInstance().getBusinessManagerSubscriptionRequest();
        InputModifyBusinessManagerSubscriptionRequest result = mapper.mapIn(SUBSCRIPTION_REQUEST_ID, BUSINESS_MANAGER_ID, input);
        assertNotNull(result);

        assertNotNull(result.getSubscriptionRequestId());
        assertNotNull(result.getBusinessManagerId());
        assertNotNull(result.getBusinessManagerSubscriptionRequest().getTargetUserId());

        assertEquals(SUBSCRIPTION_REQUEST_ID, result.getSubscriptionRequestId());
        assertEquals(BUSINESS_MANAGER_ID, result.getBusinessManagerId());
        assertEquals(input.getTargetUserId(), result.getBusinessManagerSubscriptionRequest().getTargetUserId());
    }

    @Test
    public void mapInEmptyTest() {
        InputModifyBusinessManagerSubscriptionRequest result = mapper.mapIn(SUBSCRIPTION_REQUEST_ID, BUSINESS_MANAGER_ID, new BusinessManagerSubscriptionRequest());
        assertNotNull(result);

        assertNotNull(result.getSubscriptionRequestId());
        assertNotNull(result.getBusinessManagerId());
        assertNotNull(result.getBusinessManagerSubscriptionRequest());

        assertNull(result.getBusinessManagerSubscriptionRequest().getTargetUserId());
    }
}
