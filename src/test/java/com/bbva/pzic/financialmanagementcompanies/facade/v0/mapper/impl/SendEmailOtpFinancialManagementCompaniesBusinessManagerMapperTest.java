package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.common.impl.PasswordMapper;
import com.bbva.pzic.financialmanagementcompanies.routine.PasswordGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class SendEmailOtpFinancialManagementCompaniesBusinessManagerMapperTest {

    @InjectMocks
    private SendEmailOtpFinancialManagementCompaniesBusinessManagerMapper mapper;

    @Mock
    private PasswordMapper utilMapper;

    @Mock
    private PasswordGenerator password;

    @Before
    public void setUp() {
        Mockito.when(utilMapper.mapHeader()).thenReturn(EntityStubs.getInstance().getHeaderRequestBackendContext());
        Mockito.when(utilMapper.mapBody(AAP)).thenReturn(EntityStubs.getInstance().getRequestBody());
        Mockito.when(password.generatePassword(AAP)).thenReturn("232132");
    }

    @Test
    public void testMapIn() {
        InputRequestBackendRest result = mapper.mapIn(BUSINESS_MANAGERID);

        assertNotNull(result);
        assertEquals(CONTEXT_PROVIDER_HEADER_APP, result.getHeader().getAap());
        assertEquals(CONTEXT_PROVIDER_HEADER_SERVICE_ID, result.getHeader().getServiceID());
        assertEquals(CONTEXT_PROVIDER_HEADER_REQUEST_ID, result.getHeader().getRequestID());
        assertEquals(CONTEXT_PROVIDER_HEADER_CONTACT_ID, result.getHeader().getContactID());
        assertEquals(CONTEXT_PROVIDER_HEADER_USER, result.getHeader().getUser());

        assertEquals(CONTEXT_PROVIDER_BODY_COUNTRY, result.getRequestBody().getCountry());

        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPERATION_BANK, result.getRequestBody().getDataOperationBank());
        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_LDAP, result.getRequestBody().getDataOptionalsLdap());
        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_RAMA, result.getRequestBody().getDataOptionalsRama());
        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_GRUPO, result.getRequestBody().getDataOptionalsGrupo());
        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_PREFIJO, result.getRequestBody().getDataOptionalsPrefijo());
    }

    @Test
    public void testMapInNull() {
        InputRequestBackendRest result = mapper.mapIn(null);

        assertNotNull(result);
        assertEquals(CONTEXT_PROVIDER_HEADER_SERVICE_ID, result.getHeader().getServiceID());
        assertEquals(CONTEXT_PROVIDER_HEADER_REQUEST_ID, result.getHeader().getRequestID());
        assertEquals(CONTEXT_PROVIDER_HEADER_CONTACT_ID, result.getHeader().getContactID());
        assertEquals(CONTEXT_PROVIDER_HEADER_USER, result.getHeader().getUser());

        assertEquals(CONTEXT_PROVIDER_BODY_COUNTRY, result.getRequestBody().getCountry());

        assertNull(result.getRequestBody().getDataOperationAlias());
        assertNotNull(result.getRequestBody().getDataOperationPassword());

        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPERATION_BANK, result.getRequestBody().getDataOperationBank());
        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_LDAP, result.getRequestBody().getDataOptionalsLdap());
        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_RAMA, result.getRequestBody().getDataOptionalsRama());
        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_GRUPO, result.getRequestBody().getDataOptionalsGrupo());
        assertEquals(CONTEXT_PROVIDER_BODY_DATA_OPTIONALS_PREFIJO, result.getRequestBody().getDataOptionalsPrefijo());
    }

    @Test
    public void testMapInSendMailEmpty() {
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager result = mapper.mapInSendEmail("", null, null);
        assertNotNull(result);

        assertNull(result.getEmailList());
        assertNull(result.getPassword());
        assertNull(result.getBusinessManagerId());
    }

    @Test
    public void testMapInDelUser() throws IOException {
        Mockito.when(utilMapper.mapLdapField(AAP, PasswordMapper.LDAP_DEL_USER)).thenReturn("op15");
        InputRequestBackendRest inputRequestBackendRest = EntityStubs.getInstance().getInputDelUser();
        InputRequestBackendRest result = mapper.mapInDelUser(inputRequestBackendRest);

        assertNotNull(result);
        assertNotNull(result.getRequestBody().getOperation());

        assertEquals(inputRequestBackendRest.getHeader().getAap(), result.getHeader().getAap());
        assertEquals(inputRequestBackendRest.getHeader().getServiceID(), result.getHeader().getServiceID());
        assertEquals(inputRequestBackendRest.getHeader().getRequestID(), result.getHeader().getRequestID());
        assertEquals(inputRequestBackendRest.getHeader().getContactID(), result.getHeader().getContactID());
        assertEquals(inputRequestBackendRest.getRequestBody().getCountry(), result.getRequestBody().getCountry());
        assertEquals("op15", result.getRequestBody().getOperation());

        assertEquals(inputRequestBackendRest.getRequestBody().getDataOperationAlias(), result.getRequestBody().getDataOperationAlias());
        assertEquals(inputRequestBackendRest.getRequestBody().getDataOperationBank(), result.getRequestBody().getDataOperationBank());
        assertEquals(inputRequestBackendRest.getRequestBody().getDataOptionalsLdap(), result.getRequestBody().getDataOptionalsLdap());
        assertEquals(inputRequestBackendRest.getRequestBody().getDataOptionalsRama(), result.getRequestBody().getDataOptionalsRama());
        assertEquals(inputRequestBackendRest.getRequestBody().getDataOptionalsGrupo(), result.getRequestBody().getDataOptionalsGrupo());
        assertEquals(inputRequestBackendRest.getRequestBody().getDataOptionalsPrefijo(), result.getRequestBody().getDataOptionalsPrefijo());

        assertNull(result.getRequestBody().getDataOperationPassword());
        assertNull(result.getRequestBody().getDataOperationPdgroup());
    }

    @Test
    public void testMapInDeleteGroup() throws IOException {
        Mockito.when(utilMapper.mapLdapField(AAP, PasswordMapper.LDAP_DEL_USER_FORM_GROUP)).thenReturn("op16");
        InputRequestBackendRest inputRequestBackendRest = EntityStubs.getInstance().getInputDelUser();
        InputRequestBackendRest result = mapper.mapInDelFromGroupUser(inputRequestBackendRest);

        assertNotNull(result);
        assertNotNull(result.getRequestBody().getOperation());

        assertEquals(inputRequestBackendRest.getHeader().getAap(), result.getHeader().getAap());
        assertEquals(inputRequestBackendRest.getHeader().getServiceID(), result.getHeader().getServiceID());
        assertEquals(inputRequestBackendRest.getHeader().getRequestID(), result.getHeader().getRequestID());
        assertEquals(inputRequestBackendRest.getHeader().getContactID(), result.getHeader().getContactID());
        assertEquals(inputRequestBackendRest.getHeader().getUser(), result.getHeader().getUser());
        assertEquals(inputRequestBackendRest.getRequestBody().getCountry(), result.getRequestBody().getCountry());
        assertEquals("op16", result.getRequestBody().getOperation());

        assertEquals(inputRequestBackendRest.getRequestBody().getDataOperationAlias(), result.getRequestBody().getDataOperationAlias());
        assertEquals(inputRequestBackendRest.getRequestBody().getDataOptionalsLdap(), result.getRequestBody().getDataOptionalsLdap());
        assertEquals(inputRequestBackendRest.getRequestBody().getDataOptionalsRama(), result.getRequestBody().getDataOptionalsRama());
        assertEquals(inputRequestBackendRest.getRequestBody().getDataOptionalsGrupo(), result.getRequestBody().getDataOptionalsGrupo());
        assertEquals(inputRequestBackendRest.getRequestBody().getDataOptionalsPrefijo(), result.getRequestBody().getDataOptionalsPrefijo());

        assertNull(result.getRequestBody().getDataOperationPassword());
    }

    @Test
    public void testMapInGetBusinessManagerData() throws IOException {
        InputRequestBackendRest inputRequestBackendRest = EntityStubs.getInstance().getInputDelUser();
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager result = mapper.mapInGetBusinessManagerData(BUSINESS_MANAGERID, inputRequestBackendRest);

        assertNotNull(result);
        assertNotNull(result.getBusinessManagerId());
        assertNotNull(result.getPassword());
    }

    @Test
    public void testMapInSendMailMapper() throws IOException {
        InputRequestBackendRest inputRequestBackendRest = EntityStubs.getInstance().getInputDelUser();
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager input = EntityStubs.getInstance().getInputSendEmailOtp();
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager result = mapper.mapInSendEmail(BUSINESS_MANAGERID, inputRequestBackendRest, input);

        assertNotNull(result);
        assertNotNull(result.getBusinessManagerId());
        assertNotNull(result.getPassword());
        assertNotNull(result.getEmailList());
    }

    @Test
    public void testFormat() {
        String valor = "SMCPE1810337";
        String result = String.format("smc.configuration.%s.ldapNetcash.country", valor);

        assertEquals("smc.configuration.SMCPE1810337.ldapNetcash.country", result);
    }

}
