package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestBody;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.common.IRequestBackendMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RestDeleteGroupMapperTest {

    @InjectMocks
    private RestDeleteGroupMapper mapper;

    @Mock
    private IRequestBackendMapper daoMapper;

    @Before
    public void setUp() {
        mapper = new RestDeleteGroupMapper();
        MockitoAnnotations.initMocks(this);
        mockValue();
    }

    private void mockValue() {
        Mockito.when(daoMapper.mapInBodyDataRest(Mockito.any(DTOIntRequestBody.class))).thenReturn(EntityStubs.getInstance().getBodyDataRest());
    }

    @Test
    public void mapIn() {
        InputRequestBackendRest requestBackendRest = new InputRequestBackendRest();
        requestBackendRest.setRequestBody(EntityStubs.getInstance().getRequestBodyWithPdGroup());

        BodyDataRest result = mapper.mapIn(requestBackendRest);
        assertNotNull(result);

        assertNotNull(result.getDataOperation().getPdgroup());
        assertEquals(requestBackendRest.getRequestBody().getDataOperationPdgroup(), result.getDataOperation().getPdgroup());
    }
}
