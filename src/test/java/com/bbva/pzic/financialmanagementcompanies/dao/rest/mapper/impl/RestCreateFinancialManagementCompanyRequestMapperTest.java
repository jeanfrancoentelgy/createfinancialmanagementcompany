package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyData;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy
 */

public class RestCreateFinancialManagementCompanyRequestMapperTest {

    private IRestCreateFinancialManagementCompanyRequestMapper mapper;

    @Before
    public void setUp() {
        mapper = new RestCreateFinancialManagementCompanyRequestMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        DTOIntFinancialManagementCompanies dtoIntFinancialManagementCompanies = EntityStubs.getInstance().getDTOIntFinancialManagementCompanies();
        ModelFinancialManagementCompanyRequest result = mapper.mapIn(dtoIntFinancialManagementCompanies);
        assertNotNull(result);
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getLimitAmount().getAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
        assertNotNull(result.getNetcashType().getId());
        assertNotNull(result.getNetcashType().getVersion().getId());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getProductType().getId());
        assertNotNull(result.getRelationType().getId());
        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getUnitManagement());
        assertNotNull(result.getReviewers().get(0).getBank().getId());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
        assertNotNull(result.getReviewers().get(0).getProfile().getId());
        assertNotNull(result.getReviewers().get(0).getProfessionPosition());
        assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());

    }

    @Test
    public void mapInEmptyTest(){
        DTOIntFinancialManagementCompanies dtoIntFinancialManagementCompanies = new DTOIntFinancialManagementCompanies();
        ModelFinancialManagementCompanyRequest result = mapper.mapIn(dtoIntFinancialManagementCompanies);
        assertNotNull(result);
        assertNull(result.getBusiness());
        assertNull(result.getNetcashType());
        assertNull(result.getContract());
        assertNull(result.getProduct());
        assertNull(result.getRelationType());
        assertNull(result.getReviewers());

    }

    @Test
    public void mapOutFullTest() {
        ModelFinancialManagementCompanyResponse response = ResponseFinancialManagementCompaniesMock.getInstance().getModelFinancialManagementCompanyResponse();
        FinancialManagementCompanies result = mapper.mapOut(response);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(response.getData().getId(), result.getId());
    }

    @Test
    public void mapOutEmptyTest() {
        FinancialManagementCompanies result = mapper.mapOut(null);
        assertNull(result);
    }


}
