package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifySubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestModifySubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public class RestModifySubscriptionRequestMapperTest {

    private IRestModifySubscriptionRequestMapper mapper;

    @Before
    public void setUp() {
        mapper = new RestModifySubscriptionRequestMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        final InputModifySubscriptionRequest input = EntityStubs.getInstance().getInputModifySubscriptionRequest();
        final ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getSubscriptionRequestId());
        Assert.assertNotNull(result.getStatus()
                .getId());
        Assert.assertNotNull(result.getStatus()
                .getReason());
        Assert.assertNotNull(result.getComment());
        Assert.assertEquals(input.getSubscriptionRequestId(),
                result.getSubscriptionRequestId());
        Assert.assertEquals(input.getSubscriptionRequest().getStatus().getId(), result.getStatus().getId());
        Assert.assertEquals(input.getSubscriptionRequest().getStatus().getReason(), result.getStatus().getReason());
        Assert.assertEquals(input.getSubscriptionRequest().getComment(), result.getComment());
    }

    @Test
    public void mapInEmptyTest() {
        final InputModifySubscriptionRequest input = new InputModifySubscriptionRequest();
        input.setSubscriptionRequest(new SubscriptionRequest());

        final ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getSubscriptionRequestId());
        Assert.assertNull(result.getStatus());
        Assert.assertNull(result.getComment());
    }


    @Test
    public void mapInPathFullTest() throws IOException {
        final InputModifySubscriptionRequest input = EntityStubs.getInstance().getInputModifySubscriptionRequest();
        final Map<String, String> result = mapper.mapInPath(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.get(RestModifySubscriptionRequestMapper.SUBSCRIPTION_REQUEST_ID));
        Assert.assertEquals(input.getSubscriptionRequestId(),
                result.get(RestModifySubscriptionRequestMapper.SUBSCRIPTION_REQUEST_ID));
    }

    @Test
    public void mapInPathsEmptyTest() {
        final InputModifySubscriptionRequest input = new InputModifySubscriptionRequest();
        final Map<String, String> result = mapper.mapInPath(input);
        Assert.assertNotNull(result);
        Assert.assertNull(result.get(RestModifySubscriptionRequestMapper.SUBSCRIPTION_REQUEST_ID));
    }
}
