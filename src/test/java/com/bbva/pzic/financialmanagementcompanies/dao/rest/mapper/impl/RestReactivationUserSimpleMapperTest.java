package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestBody;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.common.IRequestBackendMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class RestReactivationUserSimpleMapperTest {

    @InjectMocks
    private RestReactivationUserSimpleMapper mapper;

    @Mock
    private IRequestBackendMapper daoMapper;

    @Before
    public void setUp() {
        mapper = new RestReactivationUserSimpleMapper();
        MockitoAnnotations.initMocks(this);
        mockValue();
    }

    private void mockValue() {
        Mockito.when(daoMapper.mapInBodyDataRest(Mockito.any(DTOIntRequestBody.class))).thenReturn(EntityStubs.getInstance().getBodyDataRest());
    }

    @Test
    public void testMapIn() {
        InputRequestBackendRest requestBackendRest = new InputRequestBackendRest();
        requestBackendRest.setRequestBody(EntityStubs.getInstance().getRequestBody());
        requestBackendRest.getRequestBody().setDataOperationPdgroup("entelgy");
        requestBackendRest.getRequestBody().setDataOperationNewpassword("clave1234");

        BodyDataRest result = mapper.mapIn(requestBackendRest);

        Assert.assertNotNull(result);
        Assert.assertNull(result.getDataOperation().getPdgroup());

        Assert.assertNotNull(result.getDataOperation().getNewpassword());
        Assert.assertEquals(requestBackendRest.getRequestBody().getDataOperationNewpassword(), result.getDataOperation().getNewpassword());
    }
}
