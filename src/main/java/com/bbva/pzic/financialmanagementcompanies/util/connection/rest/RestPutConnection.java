package com.bbva.pzic.financialmanagementcompanies.util.connection.rest;

import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;
import com.bbva.pzic.financialmanagementcompanies.util.connection.RestConnectionProcessor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 22/06/2016.
 *
 * @author Entelgy
 */
public abstract class RestPutConnection<P, S> extends RestConnectionProcessor {

    private static final Log LOG = LogFactory.getLog(RestPutConnection.class);

    protected S connect(final String urlPropertyValue, final P entityPayload) {
        return connect(urlPropertyValue, null, null, null, entityPayload);
    }

    protected S connect(final String urlPropertyValue, final Map<String, String> pathParams, final P entityPayload) {
        return connect(urlPropertyValue, pathParams, null, null, entityPayload);
    }

    protected S connect(final String urlPropertyValue, final Map<String, String> pathParams, final HashMap<String, String> queryParams, final P entityPayload) {
        return connect(urlPropertyValue, pathParams, queryParams, null, entityPayload);
    }

    protected S connect(final String urlPropertyValue, final Map<String, String> pathParams, final HashMap<String, String> queryParams, final Map<String, String> headers, final P entityPayload) {
        String url = getProperty(urlPropertyValue);
        String payload = buildPayload(entityPayload);

        if (pathParams != null) {
            url = replacePathParamToUrl(url, pathParams);
        }

        if (queryParams != null) {
            LOG.info("Request query params: " + Arrays.toString(queryParams.entrySet().toArray()));
        }

        RestConnectorResponse rcr = proxyRestConnector.doPut(url, queryParams, buildOptionalHeaders(headers), payload, useProxy);

        final S response = buildResponse(rcr, 1);

        evaluateResponse(response, rcr.getStatusCode());

        return response;
    }

    protected abstract void evaluateResponse(S response, int statusCode);
}
