package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "signature", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "signature", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Signature implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Customer signature power over certains operations.
     */
    private ValidationCriteria validationRights;
    /**
     * Monetary amount limit when signing operations.
     */
    private AmountLimit amountLimit;
    /**
     * Type of signatory customer.
     */
    private String signatureType;
    /**
     * Signature mode.
     */
    private String modeType;

    public String getSignatureType() { return signatureType; }

    public void setSignatureType(String signatureType) { this.signatureType = signatureType; }

    public String getModeType() { return modeType; }

    public void setModeType(String modeType) { this.modeType = modeType; }

    public ValidationCriteria getValidationRights() {
        return validationRights;
    }

    public void setValidationRights(ValidationCriteria validationRights) {
        this.validationRights = validationRights;
    }

    public AmountLimit getAmountLimit() {
        return amountLimit;
    }

    public void setAmountLimit(AmountLimit amountLimit) {
        this.amountLimit = amountLimit;
    }
}
