package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.validation.Valid;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "businessDocument", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "businessDocument", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessDocument implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Business document type.
     */
    @Valid
    private BusinessDocumentType businessDocumentType;
    /**
     * Identity document number.
     */
    private String documentNumber;
    /**
     * Country of origin of the document.
     */
    private Country country;
    /**
     * String based on ISO-8601 for specifying the date of issuance for the
     * document.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date issueDate;
    /**
     * String based on ISO-8601 for specifying the expiry date of the document.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date expirationDate;
    /**
     * Preferential indicator over business document.
     */
    private boolean isPreferential;
    /**
     * Document owner name.
     */
    private String name;

    public Country getCountry() { return country; }

    public void setCountry(Country country) { this.country = country; }

    public boolean isPreferential() { return isPreferential; }

    public void setPreferential(boolean preferential) { isPreferential = preferential; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public BusinessDocumentType getBusinessDocumentType() {
        return businessDocumentType;
    }

    public void setBusinessDocumentType(BusinessDocumentType businessDocumentType) {
        this.businessDocumentType = businessDocumentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}
