package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "financialManagementCompanies", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "financialManagementCompanies", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class FinancialManagementCompanies implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * Business information.
     */
    @Valid
    private Business business;
    /**
     * Netcash type product associated to the company.
     */
    @Valid
    private NetcashType netcashType;
    /**
     * Contract of the costumer.
     */
    private Contract contract;
    /**
     * Financial product associated to the contract.
     */
    private RelatedProduct product;
    /**
     * It establishes that this is the account linked to the reference.
     */
    private RelationType relationType;
    /**
     * Reviewer who approved the subsription request.
     */
    @Valid
    private List<ReviewerNetcash> reviewers;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public NetcashType getNetcashType() {
        return netcashType;
    }

    public void setNetcashType(NetcashType netcashType) {
        this.netcashType = netcashType;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public RelatedProduct getProduct() {
        return product;
    }

    public void setProduct(RelatedProduct product) {
        this.product = product;
    }

    public RelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(RelationType relationType) {
        this.relationType = relationType;
    }

    public List<ReviewerNetcash> getReviewers() {
        return reviewers;
    }

    public void setReviewers(List<ReviewerNetcash> reviewers) {
        this.reviewers = reviewers;
    }
}
