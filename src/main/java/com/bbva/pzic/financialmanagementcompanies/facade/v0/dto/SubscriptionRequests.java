package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "subscriptionRequests", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "subscriptionRequests", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubscriptionRequests implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * SubscriptionRequest Data List
     */
    private List<SubscriptionRequest> data;
    /**
     * Object related to the pagination
     */
    private Pagination pagination;

    public List<SubscriptionRequest> getData() {
        return data;
    }

    public void setData(List<SubscriptionRequest> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
