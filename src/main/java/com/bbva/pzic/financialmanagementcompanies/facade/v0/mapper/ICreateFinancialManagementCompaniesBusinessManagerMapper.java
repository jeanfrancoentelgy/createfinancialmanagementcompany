package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManagerId;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public interface ICreateFinancialManagementCompaniesBusinessManagerMapper {

    DTOIntBusinessManager mapIn(BusinessManager businessManager);

    ServiceResponse<BusinessManagerId> mapOut(String identifier);
}
