package com.bbva.pzic.financialmanagementcompanies.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface ISrvFinancialManagementCompaniesV0 {

    /**
     * Creation of a subscription request, contains information related to a
     * company and the product that will be contracted.
     *
     * @param subscriptionRequest payload
     * @return {@link ServiceResponse}
     */
    ServiceResponse<SubscriptionRequestId> createSubscriptionRequest(SubscriptionRequest subscriptionRequest);

    /**
     * List the generated subscription requests.
     *
     * @param subscriptionsRequestId                  filters by the subscription request unique identifier.
     * @param businessDocumentsBusinessDocumentTypeId filters by business document type identifier.
     * @param businessDocumentsDocumentNumber         filters by document number.
     * @param statusId                                filters by status identifier.
     * @param unitManagement                          unit management identifier.
     * @param branchId                                filters by branch identifier.
     * @param fromSubscriptionRequestDate             start date filter, this filter is complemented with the
     *                                                queryParam toSubscriptionRequestDate both dates establish a
     *                                                range of dates; as an answer, it will bring the subscription
     *                                                requests received in the informed range.(ISO-8601 date
     *                                                format).
     * @param toSubscriptionRequestDate               end date filter, this filter is complemented with queryParam
     *                                                fromSubscriptionRequest; both dates establish a range of
     *                                                dates; in response, it will bring the subscription requests
     *                                                received in the reported range. (ISO-8601 date format).
     * @param businessId                              filter to search for a company by its identifier.
     * @param paginationKey                           key to obtain a single page
     * @param pageSize                                number of elements per page
     * @return {@link SubscriptionRequests}
     */
    SubscriptionRequests listSubscriptionRequests(
            String subscriptionsRequestId,
            String businessDocumentsBusinessDocumentTypeId,
            String businessDocumentsDocumentNumber, String statusId,
            String unitManagement, String branchId,
            String fromSubscriptionRequestDate,
            String toSubscriptionRequestDate, String businessId,
            String expand, String paginationKey, Integer pageSize);

    /**
     * Consult information related to a subscription request.
     *
     * @param subscriptionRequestId subscriptions request identifier.
     * @param expand                expand the selected subresource. Possible values for this
     *                              parameter are: reviewers, business-managers.
     * @return {@link SubscriptionRequest}
     */
    ServiceResponse<SubscriptionRequest> getSubscriptionRequest(
            String subscriptionRequestId, String expand);

    /**
     * Modification of a subscription request.
     *
     * @param subscriptionRequestId subscriptions request identifier.
     * @param subscriptionRequest   payload
     */
    void modifySubscriptionRequest(String subscriptionRequestId,
                                   SubscriptionRequest subscriptionRequest);

    /**
     * Modification of a business manager.
     *
     * @param subscriptionRequestId              subscriptions request identifier.
     * @param businessManagerId                  business manager request identifier.
     * @param businessManagerSubscriptionRequest payload
     */
    void modifyBusinessManagerSubscriptionRequest(
            String subscriptionRequestId,
            String businessManagerId,
            BusinessManagerSubscriptionRequest businessManagerSubscriptionRequest);

    /**
     * Creation of a reviewer. Contains information related to the reviewer who
     * will manage the subscription request.
     *
     * @param subscriptionRequestId subscriptions request identifier.
     * @param reviewer              payload
     * @return {@link ServiceResponse}
     */
    ServiceResponse<ReviewerId> createReviewerSubscriptionRequest(String subscriptionRequestId,
                                                                  Reviewer reviewer);

    /**
     * Adding multiple related contracts in bulk operation.
     *
     * @param financialManagementCompanyId unique managed business identifier.
     * @param relatedContract              payload
     * @return {@link Response}
     */
    Response createFinancialManagementCompaniesRelatedContracts(
            String financialManagementCompanyId,
            List<RelatedContract> relatedContract);

    /**
     * Service for creating a business manager. This information has no relation
     * with associated company, it is the personal information from the manager.
     *
     * @param businessManager payload
     * @return {@link ServiceResponse}
     */
    ServiceResponse<BusinessManagerId> createFinancialManagementCompaniesBusinessManager(
            BusinessManager businessManager);

    /**
     * Service for adding a new association between manager and a business.
     *
     * @param financialManagementCompanyId    unique managed business identifier.
     * @param createAuthorizedBusinessManager payload
     * @return {@link ServiceResponse}
     */
    ServiceResponse<CreateAuthorizedBusinessManager> createFinancialManagementCompaniesAuthorizedBusinessManager(
            String financialManagementCompanyId,
            CreateAuthorizedBusinessManager createAuthorizedBusinessManager);

    /**
     * Request a validation through customer email.
     * Send an email to the business manager including an OTP(One-time-password).
     *
     * @param businessManagerId business manager request identifier.
     * @param businessManager   payload
     */
    void sendEmailOtpFinancialManagementCompaniesBusinessManager(String businessManagerId,
                                                                 BusinessManager businessManager);

    /**
     * Service for retrieving a service of an authorized business manager.
     *
     * @param financialManagementCompanyId unique managed business identifier.
     * @param authorizedBusinessManagerId  unique authorized business manager identifier.
     * @param profiledServiceId            unique profiled service identifier.
     * @return {@link ServiceResponse<ProfiledService>}
     */
    ServiceResponse<ProfiledService> getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService(
            String financialManagementCompanyId,
            String authorizedBusinessManagerId, String profiledServiceId);

    /**
     * Validating that the profiled service, the account and the amount
     * associated with the service, comply with the necessary validations to be
     * performed.
     *
     * @param financialManagementCompanyId unique managed business identifier.
     * @param authorizedBusinessManagerId  unique authorized business manager identifier.
     * @param profiledServiceId            unique profiled service identifier.
     * @param validateOperationFeasibility payload
     * @return {@link ServiceResponse<ValidateOperationFeasibility>}
     */
    ServiceResponse<ValidateOperationFeasibility> validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility(
            String financialManagementCompanyId,
            String authorizedBusinessManagerId, String profiledServiceId,
            ValidateOperationFeasibility validateOperationFeasibility);

    /**
     * Service for retrieving a list of contracts that an authorized business
     * manager has profiled for a certain service.
     *
     * @param financialManagementCompanyId unique managed business identifier.
     * @param authorizedBusinessManagerId  unique authorized business manager identifier.
     * @param profiledServiceId            unique profiled service identifier.
     * @param paginationKey                key to obtain a single page
     * @param pageSize                     number of elements per page
     * @return {@link List<ServiceContract>}
     */
    ServiceResponse<List<ServiceContract>> listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts(
            String financialManagementCompanyId,
            String authorizedBusinessManagerId,
            String profiledServiceId,
            String paginationKey, Integer pageSize);

    ServiceResponse<FinancialManagementCompanies> createFinancialManagementCompanies(FinancialManagementCompanies financialManagementCompanies);

}
