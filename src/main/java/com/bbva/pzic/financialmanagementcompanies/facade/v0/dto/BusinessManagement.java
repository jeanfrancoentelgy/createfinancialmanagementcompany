package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "businessManagement", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "businessManagement", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessManagement implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Criteria that administrators will have when performing operations. This
     * configuration establishes the default permissions of the manager. In case
     * of reporting the permissions at the service level, the most specific ones
     * will be predominated, that is, the permissions configured at the service
     * level predominate over those configured at the manager level.
     */
    private OperationsRights operationsRights;
    /**
     * Rights that administrators will have when performing operations related
     * with other users.
     */
    private ValidationCriteria usersAdministrationRights;
    /**
     * Management type information.
     */
    @Valid
    private ManagementType managementType;
    /**
     * Business manager status.
     */
    private String status;
    /**
     * Customer identifier within a Business for login purposes.
     */
    private String appUserId;
    /**
     * Indicates whether the business manager has profiled all the contracted products.
     */
    private Boolean isAllServicesConfigured;
    /**
     * Indicates whether the customer has access from TPPs.
     */
    private Boolean hasThirdPartyProviderAccess;
    /**
     * Associated product to reference-customer.
     */
    private Product product;

    public String getAppUserId() { return appUserId; }

    public void setAppUserId(String appUserId) { this.appUserId = appUserId; }

    public Boolean getAllServicesConfigured() { return isAllServicesConfigured; }

    public void setAllServicesConfigured(Boolean allServicesConfigured) {
        isAllServicesConfigured = allServicesConfigured;
    }

    public Boolean getHasThirdPartyProviderAccess() { return hasThirdPartyProviderAccess; }

    public void setHasThirdPartyProviderAccess(Boolean hasThirdPartyProviderAccess) {
        this.hasThirdPartyProviderAccess = hasThirdPartyProviderAccess;
    }

    public Product getProduct() { return product; }

    public void setProduct(Product product) { this.product = product; }

    public OperationsRights getOperationsRights() {
        return operationsRights;
    }

    public void setOperationsRights(OperationsRights operationsRights) {
        this.operationsRights = operationsRights;
    }

    public ValidationCriteria getUsersAdministrationRights() {
        return usersAdministrationRights;
    }

    public void setUsersAdministrationRights(
            ValidationCriteria usersAdministrationRights) {
        this.usersAdministrationRights = usersAdministrationRights;
    }

    public ManagementType getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementType managementType) {
        this.managementType = managementType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
