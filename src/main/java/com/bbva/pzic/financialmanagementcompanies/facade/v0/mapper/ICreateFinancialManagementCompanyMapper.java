package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanyId;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public interface ICreateFinancialManagementCompanyMapper {

    DTOIntFinancialManagementCompanies mapIn(FinancialManagementCompanies financialManagementCompanies);

    ServiceResponse<FinancialManagementCompanies> mapOut(FinancialManagementCompanies financialManagementCompanies);
    
}
