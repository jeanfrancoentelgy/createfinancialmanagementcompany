package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "profile", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "profile", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Profile identifier from business agent.
     */
    private String id;
    /**
     * Profile identifier from business agent.
     */
    private String description;

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
