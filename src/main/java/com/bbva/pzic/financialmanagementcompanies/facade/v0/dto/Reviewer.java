package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "reviewer", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "reviewer", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reviewer implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Indicates the code that identifies the bank reviewer.
     */
    @NotNull(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class)
    @Size(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class, max = 7)
    private String businessAgentId;
    /**
     * First name of the reviewer.
     */
    @NotNull(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class)
    @Size(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class, max = 20)
    @DatoAuditable(omitir = true)
    private String firstName;
    /**
     * Middle name of the reviewer.
     */
    @DatoAuditable(omitir = true)
    @Size(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class, max = 20)
    private String middleName;
    /**
     * Last name of the reviewer.
     */
    @NotNull(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class)
    @Size(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class, max = 25)
    @DatoAuditable(omitir = true)
    private String lastName;
    /**
     * Second last name of the reviewer.
     */
    @Size(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class, max = 25)
    @DatoAuditable(omitir = true)
    private String secondLastName;
    /**
     * List of contact details such as mail, phone number, mobile phone number.
     */
    @Valid
    @NotNull(groups = ValidationGroup.CreateReviewerSubscriptionRequest.class)
    private List<ContactDetail> contactDetails;
    /**
     * Reviewer role.
     */
    @Valid
    private ReviewerType reviewerType;

    public String getBusinessAgentId() {
        return businessAgentId;
    }

    public void setBusinessAgentId(String businessAgentId) {
        this.businessAgentId = businessAgentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public ReviewerType getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(ReviewerType reviewerType) {
        this.reviewerType = reviewerType;
    }
}
