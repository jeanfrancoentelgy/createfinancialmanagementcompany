package com.bbva.pzic.financialmanagementcompanies.business.dto;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;

import java.util.List;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntSubscriptionRequests {

    private List<SubscriptionRequest> data;
    private DTOIntPagination pagination;

    public List<SubscriptionRequest> getData() {
        return data;
    }

    public void setData(List<SubscriptionRequest> data) {
        this.data = data;
    }

    public DTOIntPagination getPagination() {
        return pagination;
    }

    public void setPagination(DTOIntPagination pagination) {
        this.pagination = pagination;
    }
}
