package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntReviewerNetcash {

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private String businessAgentId;
    @Valid
    private List<DTOIntContactDetail> contactDetails;
    @Valid
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private DTOIntReviewerType reviewerType;
    private String unitManagement;
    private DTOIntBank bank;
    private DTOIntProfile profile;
    private String professionPosition;
    private String registrationIdentifier;

    public String getBusinessAgentId() {
        return businessAgentId;
    }

    public void setBusinessAgentId(String businessAgentId) {
        this.businessAgentId = businessAgentId;
    }

    public DTOIntReviewerType getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(DTOIntReviewerType reviewerType) {
        this.reviewerType = reviewerType;
    }

    public String getUnitManagement() {
        return unitManagement;
    }

    public void setUnitManagement(String unitManagement) {
        this.unitManagement = unitManagement;
    }

    public DTOIntBank getBank() {
        return bank;
    }

    public void setBank(DTOIntBank bank) {
        this.bank = bank;
    }

    public DTOIntProfile getProfile() {
        return profile;
    }

    public void setProfile(DTOIntProfile profile) {
        this.profile = profile;
    }

    public String getProfessionPosition() {
        return professionPosition;
    }

    public void setProfessionPosition(String professionPosition) {
        this.professionPosition = professionPosition;
    }

    public String getRegistrationIdentifier() {
        return registrationIdentifier;
    }

    public void setRegistrationIdentifier(String registrationIdentifier) {
        this.registrationIdentifier = registrationIdentifier;
    }

    public List<DTOIntContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<DTOIntContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

}