package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntLimitAmount {

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private BigDecimal amount;
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}