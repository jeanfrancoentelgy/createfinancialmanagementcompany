package com.bbva.pzic.financialmanagementcompanies.business.dto;

import com.google.api.client.util.Data;
import org.bouncycastle.util.test.FixedSecureRandom;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntFinancialManagementCompanies {

    @Valid
    private DTOIntBusiness business;
    @Valid
    private DTOIntNetcashType netcashType;
    @Valid
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private DTOIntContract contract;
    @Valid
    private DTOIntRelatedProduct product;
    @Valid
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private DTOIntRelationType relationType;
    @Valid
    private List<DTOIntReviewerNetcash> reviewers;

    public DTOIntBusiness getBusiness() {
        return business;
    }

    public void setBusiness(DTOIntBusiness business) {
        this.business = business;
    }

    public DTOIntNetcashType getNetcashType() {
        return netcashType;
    }

    public void setNetcashType(DTOIntNetcashType netcashType) {
        this.netcashType = netcashType;
    }

    public DTOIntContract getContract() {
        return contract;
    }

    public void setContract(DTOIntContract contract) {
        this.contract = contract;
    }

    public DTOIntRelatedProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntRelatedProduct product) {
        this.product = product;
    }

    public DTOIntRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(DTOIntRelationType relationType) {
        this.relationType = relationType;
    }

    public List<DTOIntReviewerNetcash> getReviewers() {
        return reviewers;
    }

    public void setReviewers(List<DTOIntReviewerNetcash> reviewers) {
        this.reviewers = reviewers;
    }
}