package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.NotNull;

public class    DTOIntContactDetail {

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private String contact;

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private String contactType;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

}
