package com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo;

public class BodyDataRest {

    private String operation;
    private String country;
    private DataOperation dataOperation;
    private DataOptionals dataOptionals;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public DataOperation getDataOperation() {
        return dataOperation;
    }

    public void setDataOperation(DataOperation dataOperation) {
        this.dataOperation = dataOperation;
    }

    public DataOptionals getDataOptionals() {
        return dataOptionals;
    }

    public void setDataOptionals(DataOptionals dataOptionals) {
        this.dataOptionals = dataOptionals;
    }
}
