package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany;

/**
 * Created on 16/09/2020.
 *
 * @author Entelgy
 */
public class ModelFinancialManagementCompanyData {

    private String id;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }
}
