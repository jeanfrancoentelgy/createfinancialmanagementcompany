package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.Contract;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.RelatedProduct;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.RelationType;

/**
 * @author Entelgy
 */
public class ModelRelatedContract {

    private Contract contract;
    private RelatedProduct product;
    private RelationType relationType;

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public RelatedProduct getProduct() {
        return product;
    }

    public void setProduct(RelatedProduct product) {
        this.product = product;
    }

    public RelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(RelationType relationType) {
        this.relationType = relationType;
    }
}
