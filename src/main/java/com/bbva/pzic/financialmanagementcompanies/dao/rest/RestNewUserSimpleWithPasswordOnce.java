package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.dao.model.validator.ResponseDataValidator;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestNewUserSimpleMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.Map;

/**
 * Created on 26/09/2018.
 *
 * @author Entelgy
 */
@Component
public class RestNewUserSimpleWithPasswordOnce extends RestPostConnection<BodyDataRest, ResponseData> {

    private static final String FINANCIAL_MANAGEMENT_COMPANY_NEW_USER_SIMPLE_WITH_PASSWORD_ONCE = "servicing.url.financialManagementCompanies.sendEmailOtpBusinessManager.newUserSimpleWithPasswordOnce";
    private static final String FINANCIAL_MANAGEMENT_COMPANY_NEW_USER_SIMPLE_WITH_PASSWORD_ONCE_USE_PROXY = "servicing.proxy.financialManagementCompanies.sendEmailOtpBusinessManager.newUserSimpleWithPasswordOnce";

    @Resource
    private ResponseDataValidator restStatusValidator;

    @Resource(name = "newUserSimpleMapper")
    private IRestNewUserSimpleMapper newUserSimpleMapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(FINANCIAL_MANAGEMENT_COMPANY_NEW_USER_SIMPLE_WITH_PASSWORD_ONCE_USE_PROXY, false);
    }

    public ResponseData invoke(InputRequestBackendRest entityPayload) {
        Map<String, String> headerParams = newUserSimpleMapper.mapInHeader(entityPayload);
        BodyDataRest bodyDataRest = newUserSimpleMapper.mapIn(entityPayload);
        return connect(FINANCIAL_MANAGEMENT_COMPANY_NEW_USER_SIMPLE_WITH_PASSWORD_ONCE, null, null, headerParams, bodyDataRest);
    }

    @Override
    protected void evaluateResponse(ResponseData response, int statusCode) {
        if (!"MDO_008".equals(response.getStatusCode().trim())) {
            evaluateMessagesResponse(Collections.singletonList(
                    restStatusValidator.buildMessage(response)), "SMCPE1810337",
                    restStatusValidator.buildStatusCode(response, statusCode));
        }
    }
}
