package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Component
public class RestCreateSubscriptionRequest extends RestPostConnection<ModelSubscriptionRequest, ModelSubscriptionResponse> {

    private static final String CREATE_SUBSCRIPTION_REQUEST_URL = "servicing.url.financialManagementCompanies.createSubscriptionRequest";
    private static final String CREATE_SUBSCRIPTION_REQUEST_USE_PROXY = "servicing.proxy.financialManagementCompanies.createSubscriptionRequest";

    @Autowired
    private IRestCreateSubscriptionRequestMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(CREATE_SUBSCRIPTION_REQUEST_USE_PROXY, false);
    }

    public String invoke(final SubscriptionRequest input) {
        return mapper.mapOut(connect(CREATE_SUBSCRIPTION_REQUEST_URL, mapper.mapIn(input)));
    }

    @Override
    protected void evaluateResponse(ModelSubscriptionResponse response, int statusCode) {
        evaluateMessagesResponse(response.getMessages(), "SMCPE1810276", statusCode);
    }
}
