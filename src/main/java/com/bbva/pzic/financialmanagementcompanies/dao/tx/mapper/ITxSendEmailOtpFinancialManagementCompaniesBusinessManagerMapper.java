package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBEHF;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBSHF;

public interface ITxSendEmailOtpFinancialManagementCompaniesBusinessManagerMapper {

    FormatoKNECBEHF mapIn(InputSendEmailOtpFinancialManagementCompaniesBusinessManager input);

    InputSendEmailOtpFinancialManagementCompaniesBusinessManager mapOut(FormatoKNECBSHF formatOutput);
}
