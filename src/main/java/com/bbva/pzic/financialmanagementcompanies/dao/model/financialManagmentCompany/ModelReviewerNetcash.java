package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany;


import java.util.List;

/**
 * Created on 15/09/2020.
 *
 * @author Entelgy
 */
public class ModelReviewerNetcash {

    private String businessAgentId;
    private List<ModelContactDetail> contactDetails;
    private ModelReviewerType reviewerType;
    private String unitManagement;
    private ModelBank bank;
    private ModelProfile profile;
    private String professionPosition;
    private String registrationIdentifier;

    public List<ModelContactDetail> getContactDetails() { return contactDetails; }

    public void setContactDetails(List<ModelContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public String getBusinessAgentId() {
        return businessAgentId;
    }

    public void setBusinessAgentId(String businessAgentId) {
        this.businessAgentId = businessAgentId;
    }

    public ModelReviewerType getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(ModelReviewerType reviewerType) {
        this.reviewerType = reviewerType;
    }

    public String getUnitManagement() {
        return unitManagement;
    }

    public void setUnitManagement(String unitManagement) {
        this.unitManagement = unitManagement;
    }

    public ModelBank getBank() {
        return bank;
    }

    public void setBank(ModelBank bank) {
        this.bank = bank;
    }

    public ModelProfile getProfile() {
        return profile;
    }

    public void setProfile(ModelProfile profile) {
        this.profile = profile;
    }

    public String getProfessionPosition() {
        return professionPosition;
    }

    public void setProfessionPosition(String professionPosition) {
        this.professionPosition = professionPosition;
    }

    public String getRegistrationIdentifier() {
        return registrationIdentifier;
    }

    public void setRegistrationIdentifier(String registrationIdentifier) {
        this.registrationIdentifier = registrationIdentifier;
    }
}