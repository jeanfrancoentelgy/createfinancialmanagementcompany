package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany;

/**
 * Created on 15/09/2020.
 *
 * @author Entelgy
 */
public class ModelBusinessManagement {

    private ModelManagementType managementType;

    public ModelManagementType getManagementType() { return managementType; }

    public void setManagementType(ModelManagementType managementType) {
        this.managementType = managementType;
    }
}
