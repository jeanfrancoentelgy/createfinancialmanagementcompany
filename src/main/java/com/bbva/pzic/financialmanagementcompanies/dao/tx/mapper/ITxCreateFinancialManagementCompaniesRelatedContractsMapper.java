package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateFinancialManagementCompaniesRelatedContracts;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.FormatoKNECBEHB;

/**
 * Created on 13/09/2018.
 *
 * @author Entelgy
 */
public interface ITxCreateFinancialManagementCompaniesRelatedContractsMapper {

    FormatoKNECBEHB mapIn(
            InputCreateFinancialManagementCompaniesRelatedContracts dtoIn);
}