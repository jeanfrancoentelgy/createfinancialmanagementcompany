package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>KNECBEHF</code> de la transacci&oacute;n <code>KWHF</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECBEHF")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECBEHF {

	/**
	 * <p>Campo <code>USUID</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "USUID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 24, longitudMaxima = 24)
	private String usuid;

}