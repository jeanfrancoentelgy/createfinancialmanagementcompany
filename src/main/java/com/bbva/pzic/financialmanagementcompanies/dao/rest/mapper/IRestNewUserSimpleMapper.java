package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;

import java.util.Map;

public interface IRestNewUserSimpleMapper {

    BodyDataRest mapIn(InputRequestBackendRest entityPayload);

    Map<String, String> mapInHeader(InputRequestBackendRest input);
}
