package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntServiceContract;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListFMCAuthorizedBusinessManagersProfiledServicesContracts;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCS1;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ServiceContract;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.converter.builtin.BooleanToStringConverter;
import com.bbva.pzic.financialmanagementcompanies.util.orika.converter.builtin.LongToIntegerConverter;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Mapper
public class TxListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper
        extends ConfigurableMapper
        implements ITxListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.getConverterFactory().registerConverter(new BooleanToStringConverter());
        factory.getConverterFactory().registerConverter(new LongToIntegerConverter());

        factory.classMap(InputListFMCAuthorizedBusinessManagersProfiledServicesContracts.class, FormatoKNECLCE0.class)
                .field("financialManagementCompanyId", "codcli")
                .field("authorizedBusinessManagerId", "codusu")
                .field("profiledServiceId", "codser")
                .field("paginationKey", "idpagin")
                .field("pageSize", "tampagi")
                .register();

        factory.classMap(FormatoKNECLCS0.class, ServiceContract.class)
                .field("numcon", "id")
                .field("tipope", "contractType")
                .field("numcon", "number")
                .field("tipcon", "numberType")
                .field("indfav", "isFavourite")
                .field("alias", "alias")
                .field("divcon", "currency")
                .field("pais", "country.id")
                .field("ideban", "bank.id")
                .field("nomban", "bank.name")
                .field("ideofi", "bank.branch.id")
                .field("nomofi", "bank.branch.name")
                .field("numint", "internalContractNumber")
                .field("numext", "counterparty")
                .field("tipper", "operationRights.permissionType.id")
                .field("destip", "operationRights.permissionType.name")
                .field("podval", "operationRights.signature.validationRights.id")
                .field("despod", "operationRights.signature.validationRights.name")
                .field("limope", "operationRights.signature.amountLimit.amount")
                .field("divlim", "operationRights.signature.amountLimit.currency")
                .field("nomtitu", "holder.fullName")
                .field("saldisp", "balance.totalAmount.amount")
                .field("divcue", "balance.totalAmount.currency")
                .field("codpro", "product.id")
                .field("nompro", "product.name")
                .field("idepro", "product.productType.id")
                .field("despro", "product.productType.description")
                .register();

        factory.classMap(FormatoKNECLCS1.class, DTOIntServiceContract.class)
                .field("idpagin", "pagination.paginationKey")
                .field("tampagi", "pagination.pageSize")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormatoKNECLCE0 mapIn(
            final InputListFMCAuthorizedBusinessManagersProfiledServicesContracts dtoIn) {
        return map(dtoIn, FormatoKNECLCE0.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntServiceContract mapOut(final FormatoKNECLCS0 formatOutput,
                                        final DTOIntServiceContract dtoOut) {
        if (dtoOut.getData() == null) {
            dtoOut.setData(new ArrayList<>());
        }
        ServiceContract serviceContract = map(formatOutput, ServiceContract.class);

        if (formatOutput.getTipope() != null) {
            serviceContract.setContractType(
                    translator.translateFrontendEnumValueStrictly(
                            "profiledServices.contracts.contractType", formatOutput.getTipope()));
        }

        if (formatOutput.getTipcon() != null) {
            serviceContract.setNumberType(
                    translator.translateFrontendEnumValueStrictly(
                            "profiledServices.contracts.NumberType", formatOutput.getTipcon()));
        }

        if (formatOutput.getTipper() != null) {
            serviceContract.getOperationRights().getPermissionType().setId(
                    translator.translateFrontendEnumValueStrictly(
                            "authorizedBusinessManager.operationsRights.permissionType.id", formatOutput.getTipper()));
        }

        if (formatOutput.getPodval() != null) {
            serviceContract.getOperationRights().getSignature().getValidationRights().setId(
                    translator.translateFrontendEnumValueStrictly(
                            "authorizedBusinessManager.signature.id", formatOutput.getPodval()));
        }

        if (formatOutput.getIdepro() != null) {
            serviceContract.getProduct().getProductType().setId(
                    translator.translateFrontendEnumValueStrictly(
                            "profiledServices.contracts.product.productType", formatOutput.getIdepro()));
        }

        dtoOut.getData().add(serviceContract);
        return dtoOut;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntServiceContract mapOut2(final FormatoKNECLCS1 formatOutput,
                                         final DTOIntServiceContract dtoOut) {
        if (dtoOut.getPagination() == null) {
            map(formatOutput, dtoOut);
        }
        return dtoOut;
    }
}
