package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany;

import java.util.List;

/**
 * Created on 15/09/2020.
 *
 * @author Entelgy
 */
public class ModelFinancialManagementCompanyRequest {

    private ModelBusiness business;
    private ModelNetcashType netcashType;
    private ModelContract contract;
    private ModelRelatedProduct product;
    private ModelRelationType relationType;
    private List<ModelReviewerNetcash> reviewers;

    public ModelBusiness getBusiness() { return business; }

    public void setBusiness(ModelBusiness business) { this.business = business; }

    public ModelNetcashType getNetcashType() { return netcashType; }

    public void setNetcashType(ModelNetcashType netcashType) { this.netcashType = netcashType; }

    public ModelContract getContract() { return contract; }

    public void setContract(ModelContract contract) { this.contract = contract; }

    public ModelRelatedProduct getProduct() { return product; }

    public void setProduct(ModelRelatedProduct product) { this.product = product; }

    public ModelRelationType getRelationType() { return relationType; }

    public void setRelationType(ModelRelationType relationType) {
        this.relationType = relationType;
    }

    public List<ModelReviewerNetcash> getReviewers() { return reviewers; }

    public void setReviewers(List<ModelReviewerNetcash> reviewers) {
        this.reviewers = reviewers;
    }
}