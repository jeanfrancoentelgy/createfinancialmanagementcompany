package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany;

/**
 * Created on 15/09/2020.
 *
 * @author Entelgy
 */
public class ModelBusinessDocumentType {

    private String id;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }
}
