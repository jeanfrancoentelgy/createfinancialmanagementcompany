package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;

/**
 * Created on 15/09/2020.
 *
 * @author Entelgy
 */
public interface IRestCreateFinancialManagementCompanyRequestMapper {

    ModelFinancialManagementCompanyRequest mapIn(DTOIntFinancialManagementCompanies dtoIntFinancialManagementCompanies);

    FinancialManagementCompanies mapOut(ModelFinancialManagementCompanyResponse response);

}
