package com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;

/**
 * Created on 27/09/2018.
 *
 * @author Entelgy
 */
public class ResponseDataMockBuilder {

    private String okStatusCode;
    private String okStatusDescription;
    private String statusCodeError;
    private String statusDescriptionError;
    private String responseError;
    private String responseEmpty;

    public ResponseDataMockBuilder() {
        this.okStatusCode = "MDO_000";
        this.okStatusDescription = "OK";
    }

    public ResponseDataMockBuilder errorStatusCode(String statusCodeError) {
        this.statusCodeError = statusCodeError;
        return this;
    }

    public ResponseDataMockBuilder errorStatusDescription(String statusDescriptionError) {
        this.statusDescriptionError = statusDescriptionError;
        return this;
    }

    public ResponseDataMockBuilder errorResponse(String responseError) {
        this.responseError = responseError;
        return this;
    }

    public ResponseDataMockBuilder emptyResponse(String responseEmpty) {
        this.responseEmpty = responseEmpty;
        return this;
    }

    public ResponseData build(String dataOperationAlias) {
        ResponseData responseData = new ResponseData();

        if (responseEmpty.equalsIgnoreCase(dataOperationAlias))
            return responseData;

        if (responseError.equalsIgnoreCase(dataOperationAlias)) {
            responseData.setStatusCode(statusCodeError);
            responseData.setStatusDescription(statusDescriptionError);
        } else {
            responseData.setStatusCode(okStatusCode);
            responseData.setStatusDescription(okStatusDescription);
        }
        return responseData;
    }
}
