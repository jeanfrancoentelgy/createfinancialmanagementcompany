package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNECBSHD</code> de la transacci&oacute;n <code>KWHD</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECBSHD")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECBSHD {

	/**
	 * <p>Campo <code>USUID</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "USUID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 24, longitudMaxima = 24)
	private String usuid;

}