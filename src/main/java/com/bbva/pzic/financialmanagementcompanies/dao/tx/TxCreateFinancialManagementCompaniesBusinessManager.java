package com.bbva.pzic.financialmanagementcompanies.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBEHD;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBSHD;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.PeticionTransaccionKwhd;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.RespuestaTransaccionKwhd;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxCreateFinancialManagementCompaniesBusinessManagerMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
@Component("txCreateFinancialManagementCompaniesBusinessManager")
public class TxCreateFinancialManagementCompaniesBusinessManager extends SingleOutputFormat<DTOIntBusinessManager, FormatoKNECBEHD, String, FormatoKNECBSHD> {

    @Resource(name = "txCreateFinancialManagementCompaniesBusinessManagerMapper")
    private ITxCreateFinancialManagementCompaniesBusinessManagerMapper mapper;

    public TxCreateFinancialManagementCompaniesBusinessManager(@Qualifier("transaccionKwhd") InvocadorTransaccion<PeticionTransaccionKwhd, RespuestaTransaccionKwhd> transaction) {
        super(transaction, PeticionTransaccionKwhd::new, String::new, FormatoKNECBSHD.class);
    }

    @Override
    protected FormatoKNECBEHD mapInput(DTOIntBusinessManager dtoIntBusinessManager) {
        return mapper.mapIn(dtoIntBusinessManager);
    }

    @Override
    protected String mapFirstOutputFormat(FormatoKNECBSHD formatoKNECBSHD, DTOIntBusinessManager dtoIntBusinessManager, String s) {
        return mapper.mapOut(formatoKNECBSHD);
    }
}