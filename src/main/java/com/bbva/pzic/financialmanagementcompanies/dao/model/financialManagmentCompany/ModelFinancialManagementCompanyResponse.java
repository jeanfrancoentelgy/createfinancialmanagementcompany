package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany;

/**
 * Created on 16/09/2020.
 *
 * @author Entelgy
 */
public class ModelFinancialManagementCompanyResponse {

    private ModelFinancialManagementCompanyData data;

    public ModelFinancialManagementCompanyData getData() { return data; }

    public void setData(ModelFinancialManagementCompanyData data) { this.data = data; }
}
