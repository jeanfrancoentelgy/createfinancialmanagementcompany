package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf;

import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNECBSHF</code> de la transacci&oacute;n <code>KWHF</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECBSHF")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECBSHF {

	/**
	 * <p>Campo <code>NOMEMP</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NOMEMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String nomemp;

	/**
	 * <p>Campo <code>USUIDS</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "USUIDS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 24, longitudMaxima = 24)
	private String usuids;

	/**
	 * <p>Campo <code>NOMUSU</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "NOMUSU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String nomusu;

	/**
	 * <p>Campo <code>DOCID</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "DOCID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String docid;

	/**
	 * <p>Campo <code>DESCID</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "DESCID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
	private String descid;

	/**
	 * <p>Campo <code>TIPUS1</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "TIPUS1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipus1;

	/**
	 * <p>Campo <code>TIPUS2</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "TIPUS2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipus2;

	/**
	 * <p>Campo <code>TIPUS3</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "TIPUS3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipus3;

	/**
	 * <p>Campo <code>TIPCO0</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "TIPCO0", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipco0;

	/**
	 * <p>Campo <code>DACO0</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "DACO0", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 78, longitudMaxima = 78)
	private String daco0;

	/**
	 * <p>Campo <code>TIPCO1</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "TIPCO1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipco1;

	/**
	 * <p>Campo <code>DACO1</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "DACO1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 78, longitudMaxima = 78)
	private String daco1;

	/**
	 * <p>Campo <code>TIPCO2</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "TIPCO2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipco2;

	/**
	 * <p>Campo <code>DACO2</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "DACO2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 78, longitudMaxima = 78)
	private String daco2;

	/**
	 * <p>Campo <code>TIPCO3</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "TIPCO3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipco3;

	/**
	 * <p>Campo <code>DACO3</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "DACO3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 78, longitudMaxima = 78)
	private String daco3;

	/**
	 * <p>Campo <code>FECALT</code>, &iacute;ndice: <code>17</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 17, nombre = "FECALT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecalt;

	/**
	 * <p>Campo <code>HORALT</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "HORALT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String horalt;

}