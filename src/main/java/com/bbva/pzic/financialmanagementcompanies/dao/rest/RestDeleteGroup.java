package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.dao.model.validator.ResponseDataValidator;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestDeleteGroupMaper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.Map;

/**
 * Created on 26/09/2018.
 *
 * @author Entelgy
 */
@Component
public class RestDeleteGroup extends RestPostConnection<BodyDataRest, ResponseData> {

    private static final String FINANCIAL_MANAGEMENT_COMPANY_DELETE_USER_GROUP = "servicing.url.financialManagementCompanies.sendEmailOtpBusinessManager.deleteUserGroup";
    private static final String FINANCIAL_MANAGEMENT_COMPANY_DELETE_USER_GROUP_USE_PROXY = "servicing.proxy.financialManagementCompanies.sendEmailOtpBusinessManager.deleteUserGroup";

    @Resource
    private ResponseDataValidator restStatusValidator;

    @Resource(name = "deleteGroupMapper")
    private IRestDeleteGroupMaper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(FINANCIAL_MANAGEMENT_COMPANY_DELETE_USER_GROUP_USE_PROXY, false);
    }

    public void invoke(InputRequestBackendRest entityPayload) {
        Map<String, String> headerParams = mapper.mapInHeader(entityPayload);
        BodyDataRest bodyDataRest = mapper.mapIn(entityPayload);
        connect(FINANCIAL_MANAGEMENT_COMPANY_DELETE_USER_GROUP ,null, null, headerParams, bodyDataRest);
    }

    @Override
    protected void evaluateResponse(ResponseData response, int statusCode) {
        evaluateMessagesResponse(Collections.singletonList(
                restStatusValidator.buildMessage(response)), "SMCPE1810337",
                restStatusValidator.buildStatusCode(response, statusCode));
    }
}
