package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class RestCreateFinancialManagementCompanyRequest extends RestPostConnection<ModelFinancialManagementCompanyRequest, ModelFinancialManagementCompanyResponse> {

    private static final String URL_PROPERTY = "servicing.smc.configuration.SMCPE1810335.backend.url";
    private static final String URL_PROPERTY_PROXY = "servicing.smc.configuration.SMCPE1810335.backend.proxy";

    @Autowired
    private IRestCreateFinancialManagementCompanyRequestMapper mapper;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator){
        this.translator = translator;
    }

    @PostConstruct
    public void init() {
        useProxy = Boolean.parseBoolean(translator.translate(URL_PROPERTY_PROXY,"false"));
    }

    public FinancialManagementCompanies invoke(final DTOIntFinancialManagementCompanies dtoInt) {
        return mapper.mapOut(connect(URL_PROPERTY, mapper.mapIn(dtoInt)));
    }

    @Override
    protected void evaluateResponse(ModelFinancialManagementCompanyResponse response, int statusCode) {

    }
}
