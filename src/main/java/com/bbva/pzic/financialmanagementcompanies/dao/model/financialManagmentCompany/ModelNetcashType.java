package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany;

/**
 * Created on 15/09/2020.
 *
 * @author Entelgy
 */
public class ModelNetcashType {

    private String id;
    private ModelVersion version;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public ModelVersion getVersion() { return version; }

    public void setVersion(ModelVersion version) { this.version = version; }
}
