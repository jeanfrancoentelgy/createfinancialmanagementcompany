package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequests;

import java.util.HashMap;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface IRestListSubscriptionRequestsMapper {

    HashMap<String, String> mapIn(InputListSubscriptionRequests input);

    DTOIntSubscriptionRequests mapOut(ModelSubscriptionRequests modelResponse);
}