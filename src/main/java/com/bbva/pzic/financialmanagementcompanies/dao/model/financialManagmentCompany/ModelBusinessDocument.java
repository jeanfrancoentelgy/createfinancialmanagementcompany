package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany;

import java.util.Date;

/**
 * Created on 15/09/2020.
 *
 * @author Entelgy
 */
public class ModelBusinessDocument {

    private ModelBusinessDocumentType businessDocumentType;
    private String documentNumber;
    private Date issueDate;
    private Date expirationDate;

    public ModelBusinessDocumentType getBusinessDocumentType() {
        return businessDocumentType;
    }

    public void setBusinessDocumentType(ModelBusinessDocumentType businessDocumentType) {
        this.businessDocumentType = businessDocumentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Date getIssueDate() {
        if (issueDate == null) {
            return null;
        }
        return new Date(issueDate.getTime());
    }

    public void setIssueDate(Date issueDate) {
        if (issueDate == null) {
            this.issueDate = null;
        } else {
            this.issueDate = new Date(issueDate.getTime());
        }
    }

    public Date getExpirationDate() {
        if (expirationDate == null) {
            return null;
        }
        return new Date(expirationDate.getTime());
    }

    public void setExpirationDate(Date expirationDate) {
        if (expirationDate == null) {
            this.expirationDate = null;
        } else {
            this.expirationDate = new Date(expirationDate.getTime());
        }
    }
}