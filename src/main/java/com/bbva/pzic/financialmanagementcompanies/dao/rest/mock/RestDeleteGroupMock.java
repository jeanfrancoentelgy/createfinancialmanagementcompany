package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.mock.ResponseDataMockBuilder;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestDeleteGroup;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 26/09/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestDeleteGroupMock extends RestDeleteGroup {

    public static final String EMPTY_RESPONSE = "66662";
    private static final String ERROR_STATUS_CODE = "MDO_003";
    private static final String ERROR_STATUS_DESCRIPTION = "Error desconocido";
    private static final String ERROR_RESPONSE = "66663";

    private ResponseDataMockBuilder responseDataMockBuilder;

    @Override
    public void init() {
        super.init();
        responseDataMockBuilder = new ResponseDataMockBuilder()
                .errorStatusCode(ERROR_STATUS_CODE)
                .errorStatusDescription(ERROR_STATUS_DESCRIPTION)
                .errorResponse(ERROR_RESPONSE)
                .emptyResponse(EMPTY_RESPONSE);
    }

    @Override
    public ResponseData connect(String urlPropertyValue, Map<String, String> pathParams, HashMap<String, String> params, Map<String, String> headers, BodyDataRest entityPayload) {
        ResponseData responseData = responseDataMockBuilder.build(entityPayload.getDataOperation().getAlias());
        evaluateResponse(responseData, 200);
        return responseData;
    }
}
