package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionResponse;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface IRestCreateSubscriptionRequestMapper {

    ModelSubscriptionRequest mapIn(SubscriptionRequest subscriptionRequest);

    String mapOut(ModelSubscriptionResponse response);
}
