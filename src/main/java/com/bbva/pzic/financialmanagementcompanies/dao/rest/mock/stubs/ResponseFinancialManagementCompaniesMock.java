package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.ErrorSeverity;
import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail.ResponseSendMail;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.*;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 15/02/2018.
 *
 * @author Entelgy
 */
public class ResponseFinancialManagementCompaniesMock {

    private static final ResponseFinancialManagementCompaniesMock INSTANCE = new ResponseFinancialManagementCompaniesMock();

    private static final String FAKE_ID = "AB01CD23EF45GH";

    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private ResponseFinancialManagementCompaniesMock() {
    }

    public static ResponseFinancialManagementCompaniesMock getInstance() {
        return INSTANCE;
    }

    public ModelSubscriptionRequestData buildResponseModelSubscriptionRequestData() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/financialmanagementcompanies/dao/rest/mock/responseModelSubscriptionRequestData.json"), ModelSubscriptionRequestData.class);
    }

    public ModelSubscriptionRequests buildResponseModelSubscriptionRequests() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/financialmanagementcompanies/dao/rest/mock/responseModelSubscriptionRequests.json"), ModelSubscriptionRequests.class);
    }

    public ModelSubscriptionResponse buildModelSubscriptionResponse() {
        ModelSubscriptionResponse response = new ModelSubscriptionResponse();
        response.setId(FAKE_ID);
        return response;
    }

    public ResponseSendMail buildSenMailResponse() {
        ResponseSendMail response = new ResponseSendMail();
        response.setTransactionID("83c5055f-3a31-4e61-b752-8f52617bd00e");
        response.setCode("202");
        response.setMessage("Correo aceptado");
        return response;
    }

    public ModelReviewerSubscriptionResponse buildModelReviewerSubscriptionResponse() {
        ModelReviewerSubscriptionResponse response = new ModelReviewerSubscriptionResponse();
        response.setParticipant(new ModelParticipant());
        response.getParticipant().setCode("");
        return response;
    }

    public ModelFinancialManagementCompanyResponse buildModelFinancialManagementCompanyResponse() {
        ModelFinancialManagementCompanyResponse modelFinancialManagementCompanyResponse = new ModelFinancialManagementCompanyResponse();
        ModelFinancialManagementCompanyData data = new ModelFinancialManagementCompanyData();
        data.setId("123");
        modelFinancialManagementCompanyResponse.setData(data);
        return modelFinancialManagementCompanyResponse;
    }

    public com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyResponse getModelFinancialManagementCompanyResponse() {
        com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyResponse modelFinancialManagementCompanyResponse = new com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyResponse();
        com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyData data = new com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagmentCompany.ModelFinancialManagementCompanyData();
        data.setId("00100121");
        modelFinancialManagementCompanyResponse.setData(data);
        return modelFinancialManagementCompanyResponse;
    }

    public Message getFatalMessage() {
        Message message = new Message();
        message.setCode("99");
        message.setMessage("Mensaje Fatal");
        message.setType(ErrorSeverity.FATAL);
        return message;
    }

    public Message getErrorMessage() {
        Message message = new Message();
        message.setCode("98");
        message.setMessage("Mensaje de Error");
        message.setType(ErrorSeverity.ERROR);
        return message;
    }
}
