package com.bbva.pzic.financialmanagementcompanies.dao.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.IFinancialManagementCompaniesDAO;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.*;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Repository
public class FinancialManagementCompaniesDAO implements IFinancialManagementCompaniesDAO {

    private static final Log LOG = LogFactory.getLog(FinancialManagementCompaniesDAO.class);

    @Autowired
    private RestCreateFinancialManagementCompanyRequest restCreateFinancialManagementCompanyRequest;
    @Autowired
    private RestCreateSubscriptionRequest restCreateSubscriptionRequest;
    @Autowired
    private RestListSubscriptionRequests restListSubscriptionRequests;
    @Autowired
    private RestGetSubscriptionRequest restGetSubscriptionRequest;
    @Autowired
    private RestModifySubscriptionRequest restModifySubscriptionRequest;
    @Autowired
    private RestModifyBusinessManagerSubscriptionRequest restModifyBusinessManagerSubscriptionRequest;
    @Autowired
    private RestCreateReviewerSubscriptionRequest restCreateReviewerSubscriptionRequest;
    @Resource(name = "txCreateFinancialManagementCompaniesBusinessManager")
    private TxCreateFinancialManagementCompaniesBusinessManager txCreateFinancialManagementCompaniesBusinessManager;
    @Resource(name = "txCreateFinancialManagementCompaniesAuthorizedBusinessManager")
    private TxCreateFinancialManagementCompaniesAuthorizedBusinessManager txCreateFinancialManagementCompaniesAuthorizedBusinessManager;
    @Resource(name = "txCreateFinancialManagementCompaniesRelatedContracts")
    private TxCreateFinancialManagementCompaniesRelatedContracts txCreateFinancialManagementCompaniesRelatedContracts;
    @Autowired
    private RestNewUserSimpleWithPasswordOnce restNewUserSimpleWithPasswordOnce;
    @Autowired
    private RestReactivationUserSimpleWithPasswordOnce restReactivationUserSimpleWithPasswordOnce;
    @Autowired
    private RestDeleteUser restDeleteUser;
    @Autowired
    private RestDeleteGroup restDeleteGroup;
    @Resource(name = "txSendEmailOtpFinancialManagementCompaniesBusinessManager")
    private TxSendEmailOtpFinancialManagementCompaniesBusinessManager txSendEmailOtpFinancialManagementCompaniesBusinessManager;
    @Autowired
    private RestSendMail restSendMail;

    @Resource(name = "txGetFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService")
    private TxGetFMCAuthorizedBusinessManagerProfiledService txGetFMCAuthorizedBusinessManagerProfiledService;
    @Resource(name = "txValidateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility")
    private TxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility txValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility;
    @Resource(name = "txListFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts")
    private TxListFMCAuthorizedBusinessManagersProfiledServicesContracts txListFMCAuthorizedBusinessManagersProfiledServicesContracts;

    /**
     * {@inheritDoc}
     */
    @Override
    public String createSubscriptionRequest(final SubscriptionRequest dtoInt) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.createSubscriptionRequest ...");
        return restCreateSubscriptionRequest.invoke(dtoInt);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntSubscriptionRequests listSubscriptionRequests(
            final InputListSubscriptionRequests input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.listSubscriptionRequests ...");
        return restListSubscriptionRequests.invoke(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubscriptionRequest getSubscriptionRequest(
            final InputGetSubscriptionRequest input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.getSubscriptionRequest ...");
        return restGetSubscriptionRequest.invoke(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifySubscriptionRequest(
            final InputModifySubscriptionRequest input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.modifySubscriptionRequest ...");
        restModifySubscriptionRequest.invoke(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyBusinessManagerSubscriptionRequest(
            final InputModifyBusinessManagerSubscriptionRequest input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.modifyBusinessManagerSubscriptionRequest ...");
        restModifyBusinessManagerSubscriptionRequest.invoke(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createReviewerSubscriptionRequest(final InputCreateReviewerSubscriptionRequest input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.createReviewerSubscriptionRequest ...");
        return restCreateReviewerSubscriptionRequest.invoke(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createFinancialManagementCompaniesRelatedContracts(
            final InputCreateFinancialManagementCompaniesRelatedContracts input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.createFinancialManagementCompaniesRelatedContracts ...");
        txCreateFinancialManagementCompaniesRelatedContracts.perform(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createFinancialManagementCompaniesBusinessManager(final DTOIntBusinessManager dtoInt) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.createFinancialManagementCompaniesBusinessManager ...");
        return txCreateFinancialManagementCompaniesBusinessManager.perform(dtoInt);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CreateAuthorizedBusinessManager createFinancialManagementCompaniesAuthorizedBusinessManager(
            final InputCreateAuthorizedBusinessManager input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.createFinancialManagementCompaniesAuthorizedBusinessManager ...");
        return txCreateFinancialManagementCompaniesAuthorizedBusinessManager.perform(input);
    }

    @Override
    public ResponseData newUserSimpleWithPasswordOnce(final InputRequestBackendRest input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.newUserSimpleWithPasswordOnce ...");
        return restNewUserSimpleWithPasswordOnce.invoke(input);
    }

    @Override
    public void sendEmail(final InputSendEmailOtpFinancialManagementCompaniesBusinessManager input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.sendEmail ...");
        restSendMail.invoke(input);
    }

    @Override
    public void reactivateUserSimpleWithPasswordOnce(final InputRequestBackendRest input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.reactivateUserSimpleWithPasswordOnce ...");
        restReactivationUserSimpleWithPasswordOnce.invoke(input);
    }

    @Override
    public InputSendEmailOtpFinancialManagementCompaniesBusinessManager getBusinessManagerData(final InputSendEmailOtpFinancialManagementCompaniesBusinessManager input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.getBusinessManagerData ...");
        return txSendEmailOtpFinancialManagementCompaniesBusinessManager.perform(input);
    }

    @Override
    public void deleteUser(final InputRequestBackendRest input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.deleteUser ...");
        restDeleteUser.invoke(input);
    }

    @Override
    public void deleteGroup(final InputRequestBackendRest input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.deleteGroup ...");
        restDeleteGroup.invoke(input);
    }

    @Override
    public ProfiledService getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService(
            final InputGetFMCAuthorizedBusinessManagerProfiledService input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService ...");
        return txGetFMCAuthorizedBusinessManagerProfiledService.perform(input);
    }

    @Override
    public ValidateOperationFeasibility validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility(
            final InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility ...");
        return txValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.perform(input);
    }

    @Override
    public DTOIntServiceContract listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts(
            final InputListFMCAuthorizedBusinessManagersProfiledServicesContracts input) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts ...");
        return txListFMCAuthorizedBusinessManagersProfiledServicesContracts.perform(input);
    }

    @Override
    public FinancialManagementCompanies createFinancialManagementCompanies(DTOIntFinancialManagementCompanies dtoInt) {
        LOG.info("... Invoking method FinancialManagementCompaniesDAO.createFinancialManagementCompanies ...");
        return restCreateFinancialManagementCompanyRequest.invoke(dtoInt);
    }


}
