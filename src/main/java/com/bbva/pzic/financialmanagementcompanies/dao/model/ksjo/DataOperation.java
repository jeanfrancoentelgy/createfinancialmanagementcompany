package com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

public class DataOperation {

    @DatoAuditable(omitir = true)
    private String alias;
    @DatoAuditable(omitir = true)
    private String password;
    private String bank;
    private String pdgroup;
    private String newpassword;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getPdgroup() {
        return pdgroup;
    }

    public void setPdgroup(String pdgroup) {
        this.pdgroup = pdgroup;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

}
