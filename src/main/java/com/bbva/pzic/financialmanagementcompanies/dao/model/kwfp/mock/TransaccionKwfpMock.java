package com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.PeticionTransaccionKwfp;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.RespuestaTransaccionKwfp;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Component("transaccionKwfp")
public class TransaccionKwfpMock implements InvocadorTransaccion<PeticionTransaccionKwfp, RespuestaTransaccionKwfp> {

    public static final String TEST_EMPTY = "66666666";
    public static final String TEST_NO_RESPONSE = "77777777";

    private FormatsKwfpMock formatsKwfpMock = FormatsKwfpMock.getInstance();

    @Override
    public RespuestaTransaccionKwfp invocar(PeticionTransaccionKwfp transaccion) {
        RespuestaTransaccionKwfp response = new RespuestaTransaccionKwfp();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        FormatoKNECFPE0 format = transaccion.getCuerpo().getParte(FormatoKNECFPE0.class);

        if (TEST_NO_RESPONSE.equals(format.getCompaid())) {
            return response;
        }
        try {
            if (TEST_EMPTY.equals(format.getCompaid())) {
                response.getCuerpo().getPartes().add(buildCopySalida(formatsKwfpMock.getFormatoKNECFPS0Empty()));
            } else {
                response.getCuerpo().getPartes().add(buildCopySalida(formatsKwfpMock.getFormatoKNECFPS0()));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return response;
    }

    @Override
    public RespuestaTransaccionKwfp invocarCache(PeticionTransaccionKwfp transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
    }

    private CopySalida buildCopySalida(FormatoKNECFPS0 formatoKNECFPS0) {
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatoKNECFPS0);
        return copySalida;
    }
}
