package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestDeleteUserMapper;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.common.IRequestBackendMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Mapper("deleteUserMapper")
public class RestDeleteUserMapper implements IRestDeleteUserMapper {

    @Autowired
    private IRequestBackendMapper requestBackendMapper;

    @Override
    public Map<String, String> mapInHeader(InputRequestBackendRest inputRequestBackendRest) {
        return requestBackendMapper.mapInHeaderDataRest(inputRequestBackendRest.getHeader());
    }

    @Override
    public BodyDataRest mapIn(InputRequestBackendRest entityPayload) {
        return requestBackendMapper.mapInBodyDataRest(entityPayload.getRequestBody());
    }
}
