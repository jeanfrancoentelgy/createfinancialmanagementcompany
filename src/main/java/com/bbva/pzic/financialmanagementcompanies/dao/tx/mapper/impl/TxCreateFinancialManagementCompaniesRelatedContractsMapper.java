package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateFinancialManagementCompaniesRelatedContracts;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.FormatoKNECBEHB;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxCreateFinancialManagementCompaniesRelatedContractsMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;

/**
 * Created on 13/09/2018.
 *
 * @author Entelgy
 */
@Mapper
public class TxCreateFinancialManagementCompaniesRelatedContractsMapper extends ConfigurableMapper implements ITxCreateFinancialManagementCompaniesRelatedContractsMapper {

    /**
     * {@inheritDoc}
     */
    @Override
    public FormatoKNECBEHB mapIn(final InputCreateFinancialManagementCompaniesRelatedContracts dtoIn) {
        return map(dtoIn, FormatoKNECBEHB.class);
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.classMap(InputCreateFinancialManagementCompaniesRelatedContracts.class, FormatoKNECBEHB.class)
                .field("financialManagementCompanyId", "codemp")
                .field("relatedContracts[0].contract.id", "cta01")
                .field("relatedContracts[0].product.id", "idepr1")
                .field("relatedContracts[0].product.productType.id", "idtpr1")
                .field("relatedContracts[0].relationType.id", "idrpr1")
                .field("relatedContracts[1].contract.id", "cta02")
                .field("relatedContracts[1].product.id", "idepr2")
                .field("relatedContracts[1].product.productType.id", "idtpr2")
                .field("relatedContracts[1].relationType.id", "idrpr2")
                .field("relatedContracts[2].contract.id", "cta03")
                .field("relatedContracts[2].product.id", "idepr3")
                .field("relatedContracts[2].product.productType.id", "idtpr3")
                .field("relatedContracts[2].relationType.id", "idrpr3")
                .field("relatedContracts[3].contract.id", "cta04")
                .field("relatedContracts[3].product.id", "idepr4")
                .field("relatedContracts[3].product.productType.id", "idtpr4")
                .field("relatedContracts[3].relationType.id", "idrpr4")
                .field("relatedContracts[4].contract.id", "cta05")
                .field("relatedContracts[4].product.id", "idepr5")
                .field("relatedContracts[4].product.productType.id", "idtpr5")
                .field("relatedContracts[4].relationType.id", "idrpr5")
                .field("relatedContracts[5].contract.id", "cta06")
                .field("relatedContracts[5].product.id", "idepr6")
                .field("relatedContracts[5].product.productType.id", "idtpr6")
                .field("relatedContracts[5].relationType.id", "idrpr6")
                .field("relatedContracts[6].contract.id", "cta07")
                .field("relatedContracts[6].product.id", "idepr7")
                .field("relatedContracts[6].product.productType.id", "idtpr7")
                .field("relatedContracts[6].relationType.id", "idrpr7")
                .field("relatedContracts[7].contract.id", "cta08")
                .field("relatedContracts[7].product.id", "idepr8")
                .field("relatedContracts[7].product.productType.id", "idtpr8")
                .field("relatedContracts[7].relationType.id", "idrpr8")
                .field("relatedContracts[8].contract.id", "cta09")
                .field("relatedContracts[8].product.id", "idepr9")
                .field("relatedContracts[8].product.productType.id", "idtpr9")
                .field("relatedContracts[8].relationType.id", "idrpr9")
                .field("relatedContracts[9].contract.id", "cta10")
                .field("relatedContracts[9].product.id", "idepr10")
                .field("relatedContracts[9].product.productType.id", "idtpr10")
                .field("relatedContracts[9].relationType.id", "idrpr10")
                .field("relatedContracts[10].contract.id", "cta11")
                .field("relatedContracts[10].product.id", "idepr11")
                .field("relatedContracts[10].product.productType.id", "idtpr11")
                .field("relatedContracts[10].relationType.id", "idrpr11")
                .field("relatedContracts[11].contract.id", "cta12")
                .field("relatedContracts[11].product.id", "idepr12")
                .field("relatedContracts[11].product.productType.id", "idtpr12")
                .field("relatedContracts[11].relationType.id", "idrpr12")
                .field("relatedContracts[12].contract.id", "cta13")
                .field("relatedContracts[12].product.id", "idepr13")
                .field("relatedContracts[12].product.productType.id", "idtpr13")
                .field("relatedContracts[12].relationType.id", "idrpr13")
                .field("relatedContracts[13].contract.id", "cta14")
                .field("relatedContracts[13].product.id", "idepr14")
                .field("relatedContracts[13].product.productType.id", "idtpr14")
                .field("relatedContracts[13].relationType.id", "idrpr14")
                .field("relatedContracts[14].contract.id", "cta15")
                .field("relatedContracts[14].product.id", "idepr15")
                .field("relatedContracts[14].product.productType.id", "idtpr15")
                .field("relatedContracts[14].relationType.id", "idrpr15")
                .register();
    }
}