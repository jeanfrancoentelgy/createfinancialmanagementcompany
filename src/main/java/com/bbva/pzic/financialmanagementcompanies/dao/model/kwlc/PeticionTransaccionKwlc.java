package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>KWLC</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionKwlc</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionKwlc</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.KWLC.D1200221.txt
 * KWLCLISTADO DE CUENTAS POR SERVICIO    KN        KN1CKWLC     01 KNECLCE0            KWLC  NS0000CNNNNN    SSTN    C   NNNSSNNN  NN                2020-02-20P017733 2020-02-2014.12.03P017733 2020-02-20-11.41.31.467365P017733 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECLCE0.D1200221.txt
 * KNECLCE0�POSICION GLOBAL-MOVIL EMPRESAS�F�05�00058�01�00001�CODCLI �CODIGO EMPRESA      �A�008�0�R�        �
 * KNECLCE0�POSICION GLOBAL-MOVIL EMPRESAS�F�05�00058�02�00009�CODUSU �CODIGO USUARIO      �A�008�0�R�        �
 * KNECLCE0�POSICION GLOBAL-MOVIL EMPRESAS�F�05�00058�03�00017�CODSER �CODIGO SERVICIO     �A�004�0�R�        �
 * KNECLCE0�POSICION GLOBAL-MOVIL EMPRESAS�F�05�00058�04�00021�IDPAGIN�IDENTIFICADOR PAGINA�A�035�0�O�        �
 * KNECLCE0�POSICION GLOBAL-MOVIL EMPRESAS�F�05�00058�05�00056�TAMPAGI�TAMA�O REG X PAGINA �N�003�0�O�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECLCS0.D1200221.txt
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�01�00001�TIPOPE �TIPO OPERATORIA     �A�002�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�02�00003�NUMCON �NUMERO DE CONTRATO  �A�020�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�03�00023�TIPCON �TIPO DE CONTRATO    �A�002�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�04�00025�INDFAV �INDICADOR FAVORITO  �A�001�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�05�00026�ALIAS  �ALIAS CONTRATO      �A�020�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�06�00046�DIVCON �DIVISA CONTRATO     �A�003�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�07�00049�PAIS   �PAIS CONTRATO       �A�003�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�08�00052�IDEBAN �IDENTIFICADOR BANCO �A�004�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�09�00056�NOMBAN �NOMBRE DE BANCO     �A�040�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�10�00096�IDEOFI �CODIGO OFICINA      �A�004�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�11�00100�NOMOFI �NOMBRE DE OFICINA   �A�040�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�12�00140�NUMINT �CONTRATO INTERNO    �A�020�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�13�00160�NUMEXT �CONTRATO EXTERNO    �A�020�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�14�00180�TIPPER �TIPO PERMISO        �A�002�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�15�00182�DESTIP �DESCRIPCION PERMISO �A�040�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�16�00222�PODVAL �PODER VALIDACION    �A�002�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�17�00224�DESPOD �DESCRIPCION PODER   �A�040�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�18�00264�LIMOPE �LIMITE CUENTA       �S�017�2�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�19�00281�DIVLIM �DIVISA LIMITE       �A�003�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�20�00284�NOMTITU�NOMBRE DE TITULAR   �A�060�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�21�00344�SALDISP�SALDO DISPONIBLE    �S�017�2�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�22�00361�DIVCUE �DIVISA CUENTA       �A�003�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�23�00364�CODPRO �CODIGO PRODUCTO     �A�002�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�24�00366�NOMPRO �NOMBRE PRODUCTO     �A�020�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�25�00386�IDEPRO �IDENT. PRODUCTO     �A�020�0�S�        �
 * KNECLCS0�POSICION GLOBAL-MOVIL EMPRESAS�X�26�00425�26�00406�DESPRO �IDENT. PRODUCTO     �A�020�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECLCS1.D1200221.txt
 * KNECLCS1�POSICION GLOBAL-MOVIL EMPRESAS�X�02�00038�01�00001�IDPAGIN�IDENTIFICADOR DE PAG�A�035�0�S�        �
 * KNECLCS1�POSICION GLOBAL-MOVIL EMPRESAS�X�02�00038�02�00036�TAMPAGI�TAMA�O DE PAGINA    �N�003�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.KWLC.D1200221.txt
 * KWLCKNECLCS0KNECLCS0KN1CKWLC1S                             P017733 2020-02-20-14.49.20.830993P017733 2020-02-20-14.49.20.831039
 * KWLCKNECLCS1KNECLCS1KN1CKWLC1S                             P017733 2020-02-20-14.51.19.137145P017733 2020-02-20-14.51.19.137177
 *
</pre></code>
 *
 * @see RespuestaTransaccionKwlc
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "KWLC",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionKwlc.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKNECLCE0.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionKwlc implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
