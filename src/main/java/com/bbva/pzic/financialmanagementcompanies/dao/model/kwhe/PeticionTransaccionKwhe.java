package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>KWHE</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionKwhe</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionKwhe</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.KWHE.D1181106.TXT
 * KWHECONF. USUARIOS NETCASH ONBOARDING  KN        KN2CKWHEPBDKNPO KNECBEHE            KWHE  NS0500NNNNNN    SSTN    C   SNNSSNNN  NN                2018-03-23XP86031 2018-09-2710.42.12P016788 2018-03-23-09.38.40.704287XP86031 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECBEHE.D1181106.TXT
 * KNECBEHE�ENTRADA ALTA CONFIG.USUARIOS  �F�05�00036�01�00001�CODEMP �CODIGO DE EMPRESA   �A�008�0�R�        �
 * KNECBEHE�ENTRADA ALTA CONFIG.USUARIOS  �F�05�00036�02�00009�USUID  �USUID               �A�024�0�R�        �
 * KNECBEHE�ENTRADA ALTA CONFIG.USUARIOS  �F�05�00036�03�00033�PODVAL �PODER DE VALIDACION �A�001�0�R�        �
 * KNECBEHE�ENTRADA ALTA CONFIG.USUARIOS  �F�05�00036�04�00034�TIPPER �TIPPER              �A�001�0�R�        �
 * KNECBEHE�ENTRADA ALTA CONFIG.USUARIOS  �F�05�00036�05�00035�ESTGER �ESTGER              �A�002�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECBSHE.D1181106.TXT
 * KNECBSHE�SALIDA ALTA CONFIG.USUARIOS   �X�04�00028�01�00001�USUIDS �USUIDS              �A�024�0�S�        �
 * KNECBSHE�SALIDA ALTA CONFIG.USUARIOS   �X�04�00028�02�00025�PODVALS�PODVALS             �A�001�0�S�        �
 * KNECBSHE�SALIDA ALTA CONFIG.USUARIOS   �X�04�00028�03�00026�TIPPERS�TIPPERS             �A�001�0�S�        �
 * KNECBSHE�SALIDA ALTA CONFIG.USUARIOS   �X�04�00028�04�00027�GERNEG �GERNEG              �A�002�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.KWHE.D1181106.TXT
 * KWHEKNECBSHECOPY    KN2CKWHE1S                             XP86031 2018-03-23-09.39.05.501901XP86031 2018-03-23-09.39.05.502971
</pre></code>
 *
 * @see RespuestaTransaccionKwhe
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "KWHE",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionKwhe.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKNECBEHE.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionKwhe implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}