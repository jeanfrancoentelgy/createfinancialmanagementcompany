package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Component("transaccionKwlc")
public class TransaccionKwlcMock implements InvocadorTransaccion<PeticionTransaccionKwlc, RespuestaTransaccionKwlc> {

    public static final String TEST_NO_PAGINATION = "88888888";
    public static final String TEST_NO_RESULT = "99999999";

    private FormatsKwlcMock formatsKwlcMock = FormatsKwlcMock.getInstance();

    @Override
    public RespuestaTransaccionKwlc invocar(PeticionTransaccionKwlc transaccion) {
        RespuestaTransaccionKwlc response = new RespuestaTransaccionKwlc();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        FormatoKNECLCE0 format = transaccion.getCuerpo().getParte(FormatoKNECLCE0.class);

        if (TEST_NO_RESULT.equals(format.getCodcli())) {
            return response;
        }

        try {
            response.getCuerpo().getPartes().addAll(buildCopiesSalida(formatsKwlcMock.getFormatoKNECLCS0s()));

            if (!TEST_NO_PAGINATION.equals(format.getCodcli())) {
                response.getCuerpo().getPartes().add(buildCopySalida(formatsKwlcMock.getFormatoKNECLCS1()));
            }

            return response;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public RespuestaTransaccionKwlc invocarCache(PeticionTransaccionKwlc transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
    }

    private List<CopySalida> buildCopiesSalida(List<FormatoKNECLCS0> formatoKNECLCS0s) {
        List<CopySalida> copies = new ArrayList<>();
        for (FormatoKNECLCS0 format : formatoKNECLCS0s) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }

    private CopySalida buildCopySalida(FormatoKNECLCS1 formatoKNECLCS1) {
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatoKNECLCS1);
        return copySalida;
    }
}
