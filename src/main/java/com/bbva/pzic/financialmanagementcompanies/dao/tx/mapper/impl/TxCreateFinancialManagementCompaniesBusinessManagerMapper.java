package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBEHD;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBSHD;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxCreateFinancialManagementCompaniesBusinessManagerMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
@Mapper
public class TxCreateFinancialManagementCompaniesBusinessManagerMapper extends ConfigurableMapper implements ITxCreateFinancialManagementCompaniesBusinessManagerMapper {

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.classMap(DTOIntBusinessManager.class, FormatoKNECBEHD.class)
                .field("financialManagementCompanyId", "codemp")
                .field("fullName", "nomusu")
                .field("identityDocumentDocumentTypeId", "docid")
                .field("identityDocumentDocumentNumber", "nrodid")
                .field("contactDetailsContactType0", "tipco0")
                .field("contactDetailsContact0", "daco0")
                .field("contactDetailsContactType1", "tipco1")
                .field("contactDetailsContact1", "daco1")
                .field("contactDetailsContactType2", "tipco2")
                .field("contactDetailsContact2", "daco2")
                .field("contactDetailsContactType3", "tipco3")
                .field("contactDetailsContact3", "daco3")
                .field("rolesId0", "tipus1")
                .field("rolesId1", "tipus2")
                .field("rolesId2", "tipus3")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormatoKNECBEHD mapIn(final DTOIntBusinessManager dtoIn) {
        return map(dtoIn, FormatoKNECBEHD.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String mapOut(final FormatoKNECBSHD formatOutput) {
        return formatOutput.getUsuid();
    }
}