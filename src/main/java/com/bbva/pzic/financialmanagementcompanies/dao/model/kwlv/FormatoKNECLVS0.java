package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNECLVS0</code> de la transacci&oacute;n <code>KWLV</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECLVS0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECLVS0 {

    /**
     * <p>Campo <code>INDLIMI</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 1, nombre = "INDLIMI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String indlimi;

    /**
     * <p>Campo <code>DESCLIM</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "DESCLIM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String desclim;

}
