package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateReviewerSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
@Component
public class RestCreateReviewerSubscriptionRequest extends RestPostConnection<ModelReviewerSubscriptionRequest, ModelReviewerSubscriptionResponse> {

    private static final String CREATE_REVIEWER_SUBSCRIPTION_REQUEST_URL = "servicing.url.financialManagementCompanies.createReviewerSubscriptionRequest";
    private static final String CREATE_REVIEWER_SUBSCRIPTION_REQUEST_USE_PROXY = "servicing.proxy.financialManagementCompanies.createReviewerSubscriptionRequest";

    @Autowired
    private IRestCreateReviewerSubscriptionRequestMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(CREATE_REVIEWER_SUBSCRIPTION_REQUEST_USE_PROXY, false);
    }

    public String invoke(final InputCreateReviewerSubscriptionRequest input) {
        return mapper.mapOut(connect(CREATE_REVIEWER_SUBSCRIPTION_REQUEST_URL, mapper.mapInPathParams(input), mapper.mapIn(input)));
    }

    @Override
    protected void evaluateResponse(ModelReviewerSubscriptionResponse response, int statusCode) {
        evaluateMessagesResponse(response.getMessages(), "SMCPE1810290", statusCode);
    }
}