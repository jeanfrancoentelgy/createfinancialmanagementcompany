package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestModifySubscriptionRequest;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestModifySubscriptionRequestMock extends RestModifySubscriptionRequest {

    @Override
    protected ModelSubscriptionResponse connect(String urlPropertyValue, Map<String, String> path, ModelSubscriptionRequest entityPayload) {
        return new ModelSubscriptionResponse();
    }
}